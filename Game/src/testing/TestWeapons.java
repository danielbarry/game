package testing;

import entities.weapons.Weapon;
import sprites.Sprite;
import sprites.SpriteSheet;
import utils.math.Position;

/**
 *
 * @author Oliver
 */
public class TestWeapons{

    public static void main(String[] args){
        new TestWeapons();
    }


    public TestWeapons(){
        Sprite spriteAK = new Sprite(16 * 3, 0, 0, SpriteSheet.weapons);
        Weapon ak47 = new Weapon("ak-47", 100, new Position(50, 40, 0), spriteAK, 0);
//        TestDraw.draw(ak47.getSprite());
        new TestSprites(ak47.getSprite());
    }
}
