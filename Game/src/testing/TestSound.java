package testing;

import io.InOut;
import io.Terminal;
import utils.sound.SoundManager;

/**
 *
 * @author Oliver
 */
public class TestSound{

    private static final InOut term = Terminal.getInstance();


    public static void main(String[] args) throws InterruptedException{
        SoundManager s = SoundManager.getInstance();
        s.addSound("effect.explosion");
        s.addSound("music.level_one");
        s.playSound("effect.explosion");
        s.loopSound("music.level_one");
        Thread.sleep(5000);
//        System.out.println("After sleep");
        s.stopSound("music.level_one");
    }
}
