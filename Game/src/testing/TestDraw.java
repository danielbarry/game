package testing;

import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import sprites.Sprite;

/**
 *
 * @author Oliver
 */
public class TestDraw{

    private static JFrame frame;


    public static void draw(Sprite sprite){
        setupFrame();

        //Create a new buffered image from the sprite
        BufferedImage image = new BufferedImage(frame.getWidth(), frame.getHeight(), BufferedImage.TYPE_INT_RGB);
        image.setRGB(0, 0, sprite.SIZE, sprite.SIZE, sprite.getPixels(), 0, sprite.SIZE);

        drawImage(image);
    }


    private static void setupFrame(){
        frame = new JFrame("Test Sprites");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(640, 480);
        frame.setResizable(false);
        frame.setVisible(true);
    }


    private static void drawImage(BufferedImage image){
        BufferStrategy bs = frame.getBufferStrategy();
        if(bs == null){
            frame.createBufferStrategy(3);
            bs = frame.getBufferStrategy();
        }

        Graphics2D g = (Graphics2D)bs.getDrawGraphics();
        //I don't know why these numbers work as offsets, but they do
        g.drawImage(image, 3, 26, frame.getWidth(), frame.getHeight(), null);
        g.dispose();
        bs.show();
        frame.paint(g);
    }
}
