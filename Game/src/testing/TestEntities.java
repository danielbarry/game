package testing;

import entities.mob.Player;
import entities.weapons.Weapon;
import sprites.Sprite;
import utils.math.Position;

/**
 *
 * @author Oliver
 */
public class TestEntities{

    public static void main(String[] args){
        new TestEntities();
    }
    
    public TestEntities(){
        Player player = new Player("Ollie", new Position(1, 1, 0d), new Sprite(16, 0xFF00FF));
        System.out.println("Max health:     " + player.getMaxHealth());
        System.out.println("Current health: " + player.getHealth());
        System.out.println("Damgage done:   10");
        player.removeHealth(10);
        System.out.println("------------------------");
        System.out.println("Max health:     " + player.getMaxHealth());
        System.out.println("Current health: " + player.getHealth());
        System.out.println("------------------------");
        System.out.println("Active weapon:  " + player.getActiveWeapon());
        System.out.println("------------------------");
        System.out.println("Creating a new pistol and adding it");
        Weapon pistol = new Weapon("Perry the pistol", 6, player.getPosition(), new Sprite(8, 0x00FF00), 1);
        player.pickupWeapon(pistol);
        player.getAresenal().forEach((w) -> {
            System.out.println(w);
        });
        System.out.println("Active weapon:  " + player.getActiveWeapon());
        System.out.println("------------------------");
        System.out.println("Setting the active weapon");
        player.setActiveWeapon(pistol);
        System.out.println("Active weapon:\n" + player.getActiveWeapon());
        System.out.println("------------------------");
        System.out.println(player.toString());
    }
}
