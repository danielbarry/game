package testing;

import game.achievement.Achieve;
import game.achievement.Achieve.AchievementRule;
import game.achievement.Achievement;

/**
 *
 * @author Oliver
 */
public class TestAchievements{

    private static Achieve a;


    public static void main(String[] args){
        //Example stuff
        a = Achieve.getInstance();
        initGame();
        gameLoop();
        levelUp();

        //Testing stuff
//        test();
    }


    private static void initGame(){
        a.defineProperty("Enemies killed", 0, AchievementRule.GREATER_THAN, 10);
        a.defineProperty("Lives remaining", 3, AchievementRule.EQUAL_TO, 3, "level 1");
        a.defineProperty("Levels completed", 0, AchievementRule.EQUAL_TO, 3);
        a.defineProperty("Deaths", 0, AchievementRule.EQUAL_TO, 0);

        a.defineAchievement("Master kill", a.getProperty("Enemies killed")); //Kill 10 enemies
        a.defineAchievement("Can't touch this", a.getProperty("Lives remaining")); //Survive level 1 with 3 lives
        a.defineAchievement("Win the game", a.getProperty("Levels completed")); //Complete 3 levels
        a.defineAchievement("Chuck Norris", a.getProperty("Levels completed"), a.getProperty("Deaths")); //Complete 3 levels without dying
    }


    private static void gameLoop(){
        boolean enemyWasKilled = true;
        if(enemyWasKilled){
            a.addValue(1, "Enemies killed");
        }

        boolean playerJustDied = true;
        if(playerJustDied){
            a.addValue(-1, "Lives remaining");
            a.addValue(1, "Deaths");
        }
    }


    private static void levelUp(){
        a.addValue(1, "Levels completed");

        //Loop through all just unlocked achievements, none have been unlocked because of deaths
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        //Reset all level 1 properties
        a.resetTaggedProperties("level 1");
    }


    /*
     Lots of ugly testing
     */
    private static void test(){
        a.defineProperty("Kill 10 enemies", 0, AchievementRule.EQUAL_TO, 10, "test_tag");
        a.defineProperty("Kill 4 enemies with a hammer", 0, AchievementRule.GREATER_THAN, 4);
        a.defineProperty("Die more than once", 5, AchievementRule.LESS_THAN, 4, "test_tag");

        a.defineAchievement("Killing spree", a.getProperty("Kill 10 enemies"), a.getProperty("Kill 4 enemies with a hammer"), a.getProperty("Die more than once"));

        System.out.println("All Achievements");
        for(Achievement ach : a.getAllAchievements()){
            System.out.println(ach);
        }

        System.out.println("Check 1");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        a.addValue(5, "Kill 10 enemies");
        System.out.println("Check 2");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        a.setValue(10, "Kill 4 enemies with a hammer");
        System.out.println("Check 3");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        a.addValue(5, "Kill 10 enemies");
        System.out.println("Check 4");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        a.setValue(3, "Die more than once");
        System.out.println("Check 5");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        a.resetTaggedProperties("test_tag");
        System.out.println("Check 6");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }

        a.setValue(3, "Die more than once");
        System.out.println("Check 7");
        for(Achievement ach : a.checkAchievements()){
            System.out.println(ach);
        }
    }
}
