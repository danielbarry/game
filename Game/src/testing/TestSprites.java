package testing;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import javax.swing.JFrame;
import sprites.Sprite;

/**
 *
 * @author Oliver
 */
public class TestSprites{

    private JFrame frame;


    public static void main(String[] args){
        Sprite sprite = new Sprite(64, 0x0000FF); //Blue box
//        Sprite sprite = new Sprite(16, 0, 0, SpriteSheet.tiles); //Grass texture
//        Sprite sprite = new Sprite(16 * 3, 0, 0, SpriteSheet.weapons); //Ak-47
        new TestSprites(sprite);
    }


    public TestSprites(Sprite sprite){
        setupFrame();


        //Create a new buffered image from the sprite
        BufferedImage image = new BufferedImage(frame.getWidth(), frame.getHeight(), BufferedImage.TYPE_INT_RGB);
        image.setRGB(0, 0, sprite.SIZE, sprite.SIZE, sprite.getPixels(), 0, sprite.SIZE);

        drawImage(image);
    }


    private void setupFrame(){
        frame = new JFrame("Test Sprites");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(640, 480);
        frame.setResizable(false);
        frame.setVisible(true);
    }


    private void drawImage(BufferedImage image){
        BufferStrategy bs = frame.getBufferStrategy();
        if(bs == null){
            frame.createBufferStrategy(3);
            bs = frame.getBufferStrategy();
        }

        Graphics2D g = (Graphics2D)bs.getDrawGraphics();
//        g.scale(7, 7);
        //I don't know why these numbers work as offsets, but they do
        g.drawImage(image, 3, 26, frame.getWidth(), frame.getHeight(), null);
        g.setColor(Color.WHITE);
        g.setFont(new Font("Verdana", 0, 50));
        g.drawString("Hello", 100, 100);
        g.dispose();
        bs.show();
        frame.paint(g);
    }
}
