package io.out;

/**
 * The Simple class provides a meaningful and standardized method of displaying
 * information to the terminal. In both Linux and Mac, terminal's are coloured
 * to indicate different types of status.
 * @author dan
 * @version 1.0
 */
public abstract class Simple{
    //Constant colour values for Linux and Mac
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    
    /**
     * Available status messages for debugging the program the the terminal.
     */
    public enum State{
        OK(ANSI_GREEN, "[ OK ] "),
        FAIL(ANSI_RED, "[FAIL] "),
        DEBUG(ANSI_PURPLE, "[ >> ] ");
        
        String startColour, start;
        
        /**
         * Constructor for state, with starting String and colour if the OS is
         * Linux or Mac (Unix based).
         * </ br>
         * States available are:
         * <ul>
         *   <li>OK - indicates everything is going as planned and something
         *       significant happened that the user may want to know.</li>
         *   <li>FAIL - indicates something has gone wrong. This shouldn't cause
         *       the program to crash and it should recover some how.</li>
         *   <li>DEBUG - indicates that the information being printed is
         *       important for debug purposes. This can be used for testing and
         *       is only displayed when the debug boolean is turned on.</li>
         * </ul>
         * @param startColour colour in ansi colour code format to be printed to
         * a Unix based terminal.
         * @param start String to be displayed at start of message to indicate
         * what type it is.
         */
        State(String startColour, String start){
            this.startColour = startColour;
            this.start = start;
        }
        
        /**
         * Returns the start String to indicate what type of message is being
         * printed.
         * @return message beginning indicator.
         */
        String getStart(){ return start; }
        
        /**
         * Returns a colour to be displayed if the terminal is Unix based.
         * @return a ansi colour encapsulated in a String, ready to be printed
         * to the terminal. It automatically resets the colour back to default
         * to prevent possible colouring issues.
         */
        String getColourStart(){ return startColour + start + ANSI_RESET; }
    }
    
    private static boolean debug = false;
    
    //Program constants, set to reduce any processing required later
    private static final String OS = System.getProperty("os.name").toLowerCase();
    private static final boolean LINUX_OS = OS.contains("nix") || OS.contains("nux") || OS.contains("aix");
    private static final boolean WINDOWS_OS = OS.contains("win");
    private static final boolean MAC_OS = OS.contains("mac") || OS.contains("darwin");
    private static final boolean UNKNONW_OS = !LINUX_OS && !WINDOWS_OS && !MAC_OS;
    
    /**
     * Does a simple print to System.out.print().
     * @param msg the message to be printed to the terminal.
     */
    public static void print(String msg){ System.out.print(msg); }
    
    /**
     * Adds a line break to the end of the message and uses the print() method
     * to print to the terminal.
     * @param msg the message to be printed to the terminal.
     */
    public static void println(String msg){ print(msg + System.lineSeparator()); }
    
    /**
     * Prints a message with a given status, as per the State enumerator. This
     * method is responsible for getting the colour codes if it is appropriate
     * to do so.
     * @param state the state the message represents.
     * @param msg the message to be printed with the status, giving relative
     * detail. Message should never exceed 70 characters as a guide line as this
     * would display on multiple lines and possibly be confusing.
     */
    public static void status(State state, String msg){
        if(debug){
            if(LINUX_OS||MAC_OS)println(state.getColourStart() + msg);
            else println(state.getStart() + msg);
        }
    }
    
    /**
     * Allows for debug mode to be switched on.
     * @param set set to true if debug mode is to be set on, false otherwise.
     */
    public static void debug(boolean set){ debug = set; }
    
    /**
     * Returns whether prompt is Linux.
     * @return returns true if Linux, otherwise false.
     */
    public static boolean linux(){ return LINUX_OS; }
    
    /**
     * Returns whether prompt is Windows.
     * @return returns true if Windows, otherwise false.
     */
    public static boolean windows(){ return WINDOWS_OS; }
    
    /**
     * Returns whether prompt is Mac.
     * @return returns true if Mac, otherwise false.
     */
    public static boolean mac(){ return MAC_OS; }
    
    /**
     * Returns whether prompt is Unknown.
     * @return returns true if Unknown, otherwise false.
     */
    public static boolean unknown(){ return UNKNONW_OS; }
}