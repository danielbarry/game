package io;

import entities.Entity;
import entities.mob.Player;
import game.map.Map;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import utils.loader.Loader;
import utils.updater.Updater;

public class Gui implements Runnable, InOut {

    //Windows
    private final JFrame START_WINDOW;
    private final JFrame SETTINGS_WINDOW = new JFrame("Settings " + (String)Loader.getSetting("title"));
    private static final JFrame GAME_WINDOW = new JFrame("Playing " + (String)Loader.getSetting("title"));

    //start window components
    private static JLabel titleLabel;
    private static JLabel imageLabel;
    private static final JCheckBox CHK_FULLSCREEN = new JCheckBox("FullScreen");
    private static final JComboBox CMB_RENDER_DIST = new JComboBox<>(RenderDistance.values());
    private static final JButton BTN_PLAY = new JButton("Play");
    private static final JButton BTN_CLOSE = new JButton("Close");
    private static final JButton BTN_SETTINGS = new JButton("Settings");

    //settings window components
    private static JLabel visualSettings = new JLabel("Visual Setting", JLabel.CENTER);
    private static JLabel keysSettingsLabel = new JLabel("Input Settings", JLabel.CENTER);
    private static final JComboBox CMB_MAX_UPS = new JComboBox<>(MaxUpdatesPerSecond.values());
    private static final JButton BTN_APPLY = new JButton("Apply");
    private static final JButton BTN_CANCEL = new JButton("Cancel");

    
    //referecnes to Game objects
    Map[] maps;
    Entity[] entities;
    Player player;

    public void run()
    {
        
    }
    
    public Gui(Map[] maps, Entity[] entities,Player player) {
        this.maps = maps;
        this.entities = entities;
        this.player = player;
        START_WINDOW = new JFrame((String)Loader.getSetting("setup-title") + ' ' + Updater.getVersion());

        //load the icon
        BufferedImage icon = null;
        try{
            InputStream iconStream = getClass().getResourceAsStream((String)Loader.getSetting("icon"));
            icon = ImageIO.read(iconStream);
        }catch(IOException ioe){
            err("Icon load failure");
        }

        //load the banner
        BufferedImage banner = null;
        try{
            InputStream bannerStream = getClass().getResourceAsStream((String)Loader.getSetting("banner"));
            banner = ImageIO.read(bannerStream);
        }catch(IOException ioe){
            err("Banner load failure");
        }

        //setting up action listeners
        BTN_PLAY.addActionListener(new actionEvent());
        BTN_CLOSE.addActionListener(new actionEvent());
        BTN_SETTINGS.addActionListener(new actionEvent());

        //setting up the start window
        START_WINDOW.setSize(Integer.parseInt((String)Loader.getSetting("settings-width")), Integer.parseInt((String)Loader.getSetting("settings-height")));
        START_WINDOW.setLayout(new GridLayout(5, 1));
        START_WINDOW.setIconImage(icon);
        START_WINDOW.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        imageLabel = new JLabel(new ImageIcon(banner));
        titleLabel = new JLabel((String)Loader.getSetting("title"), JLabel.CENTER);
        START_WINDOW.add(titleLabel);
        START_WINDOW.add(imageLabel);
        START_WINDOW.add(BTN_PLAY);
        START_WINDOW.add(BTN_SETTINGS);
        START_WINDOW.add(BTN_CLOSE);
        START_WINDOW.setVisible(true);

        //setting up the settings window :D
        SETTINGS_WINDOW.setSize(Integer.parseInt((String)Loader.getSetting("settings-width")), Integer.parseInt((String)Loader.getSetting("settings-height")));
        SETTINGS_WINDOW.setLayout(new GridLayout(12, 1));
        SETTINGS_WINDOW.setIconImage(icon);
        SETTINGS_WINDOW.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SETTINGS_WINDOW.add(visualSettings);
        SETTINGS_WINDOW.add(CHK_FULLSCREEN);
        SETTINGS_WINDOW.add(CMB_RENDER_DIST);
        SETTINGS_WINDOW.add(CMB_MAX_UPS);
        SETTINGS_WINDOW.add(keysSettingsLabel);
        SETTINGS_WINDOW.add(BTN_APPLY);
        SETTINGS_WINDOW.add(BTN_CANCEL);
        SETTINGS_WINDOW.setVisible(false);

        BTN_APPLY.addActionListener(new actionEvent());
        BTN_CANCEL.addActionListener(new actionEvent());
    }

    public class actionEvent implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e){
            if(e.getSource() == BTN_PLAY){
                startGame();
            }else if(e.getSource() == BTN_CLOSE){
                closeGame();
            }else if(e.getSource() == BTN_SETTINGS){
                adjustSettings();
            }else if(e.getSource() == BTN_APPLY){
                applySettings();
            }else if(e.getSource() == BTN_CANCEL){
                cancelSettings();
            }
        }
    }


    /**
     * Close the start game window and creates the Game window with given
     * settings
     */
    private void startGame(){
        if(CHK_FULLSCREEN.isSelected()){
            GAME_WINDOW.setExtendedState(Frame.MAXIMIZED_BOTH);
            GAME_WINDOW.setUndecorated(true);
            GAME_WINDOW.setFocusable(true);                 //make sure keyboard is talking to this window
        }else{
            GAME_WINDOW.setSize(Integer.parseInt((String)Loader.getSetting("game-width")), Integer.parseInt((String)Loader.getSetting("game-height")));
        }
        GAME_WINDOW.add(new Draw(maps[0], entities, player, 0, 0, 0, "high"));
        GAME_WINDOW.setVisible(true);
        GAME_WINDOW.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        START_WINDOW.setVisible(false);
    }


    private void closeGame(){
        System.exit(0);
    }


    /**
     * This method open the setting window, before displaying the settings
     * window it current setting to display them as the default selected values.
     */
    private void adjustSettings(){
        START_WINDOW.setVisible(false);

        //load the current setup
        CHK_FULLSCREEN.setSelected(Boolean.valueOf((String)Loader.getSetting("is-fullscreen")));
        switch(Integer.parseInt((String)Loader.getSetting("render-distance"))) //TODO elimenate this switch
        {
            case 10:
                CMB_RENDER_DIST.setSelectedItem(RenderDistance.RENDER_DISTANCE_NEAR);
                break;
            case 20:
                CMB_RENDER_DIST.setSelectedItem(RenderDistance.RENDER_DISTANCE_NORMAL);
                break;
            case 30:
                CMB_RENDER_DIST.setSelectedItem(RenderDistance.RENDER_DISTANCE_FAR);
                break;

            default:
                CMB_RENDER_DIST.setSelectedItem(RenderDistance.RENDER_DISTANCE_NORMAL);
        }

        switch(Integer.parseInt((String)Loader.getSetting("max-ups"))) //TODO elimenate this switch
        {
            case 5:
                CMB_MAX_UPS.setSelectedItem(MaxUpdatesPerSecond.PAINFUL);
                break;
            case 10:
                CMB_MAX_UPS.setSelectedItem(MaxUpdatesPerSecond.SLOW);
                break;
            case 20:
                CMB_MAX_UPS.setSelectedItem(MaxUpdatesPerSecond.NORMAL);
                break;
            case 30:
                CMB_MAX_UPS.setSelectedItem(MaxUpdatesPerSecond.FAST);
                break;
            case 60:
                CMB_MAX_UPS.setSelectedItem(MaxUpdatesPerSecond.ULTRA);
                break;
            default:
                CMB_MAX_UPS.setSelectedItem(MaxUpdatesPerSecond.NORMAL);
        }
        SETTINGS_WINDOW.setVisible(true);
    }


    /**
     * Commits change to the properties, this method is called by the apply
     * button
     */
    private void applySettings(){
        SETTINGS_WINDOW.setVisible(false);
        String fullscreen = "false";
        if(CHK_FULLSCREEN.isSelected()){
            fullscreen = "true";
        }else{
            fullscreen = "false";
        }
        Loader.setSetting("is-fullscreen", fullscreen);

        Loader.setSetting("render-distance", "" + ((RenderDistance)CMB_RENDER_DIST.getSelectedItem()).getTiles());
        Loader.setSetting("max-ups", "" + ((MaxUpdatesPerSecond)CMB_MAX_UPS.getSelectedItem()).getUPSRate());
        START_WINDOW.setVisible(true);
    }


    /**
     * Close the settings window and forget the changes
     */
    private void cancelSettings(){
        SETTINGS_WINDOW.setVisible(false);
        START_WINDOW.setVisible(true);
    }
    
    //TODO: Implement different types of dialog box. Some better handle
    //      "information", "errors" and debug messages. See the UpdaterGUI for
    //      how to implement this.
    
    @Override
    public void message(String msg){ JOptionPane.showMessageDialog(START_WINDOW, "Message: " + msg); }

    @Override
    public void err(String msg){ JOptionPane.showMessageDialog(START_WINDOW, "Error: " + msg); }

    @Override
    public void debug(String msg){ JOptionPane.showMessageDialog(START_WINDOW, "Debug: " + msg); }

    @Override
    public void message(Object msg){ JOptionPane.showMessageDialog(START_WINDOW, "Message: " + msg.toString()); }

    @Override
    public void err(Object msg){ JOptionPane.showMessageDialog(START_WINDOW, "Error: " + msg.toString()); }

    @Override
    public void debug(Object msg){ JOptionPane.showMessageDialog(START_WINDOW, "Debug: " + msg.toString()); }
}