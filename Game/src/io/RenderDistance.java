package io;

/**
 *
 * @author Paul
 * @version 0.1 build 1 "soft version name"
 */
public enum RenderDistance {
       
    RENDER_DISTANCE_NEAR(10, "Near"),
    RENDER_DISTANCE_NORMAL(20, "Normal"),
    RENDER_DISTANCE_FAR(30, "Far");
    
    private final int TILES;
    private final String name;
    
    RenderDistance(int tiles, String name){
        this.TILES = tiles;     
        this.name = name;
    }
    
    int getTiles(){
        return TILES;
    }
    
    @Override
    public String toString(){
        return name;
    }
}
