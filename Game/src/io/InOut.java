package io;

/**
 * This Input/Output class currently allows for messages to be displayed to the
 * user. It will also give input methods in the future for the implementing
 * class to handle and the Game to use as a method of interaction from the user.
 * <p>
 * @author dan
 * @version 1.0
 */
public interface InOut{
    /**
     * Allows for a simple message to be displayed, making the user aware that
     * either something has complete or something significant that is also good
     * has happened.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    public void message(String msg);

    /**
     * Allows a debug message to be displayed. By default this won't print until
     * it is turned on. This method should be typically used by the developer.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    public void debug(String msg);

    /**
     * Allows for an error message to be displayed. This method will not handle
     * the error in itself and must still be handled by the calling class.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    public void err(String msg);
    
    /**
     * Allows for a simple message to be displayed, making the user aware that
     * either something has complete or something significant that is also good
     * has happened.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    public void message(Object msg);

    /**
     * Allows a debug message to be displayed. By default this won't print until
     * it is turned on. This method should be typically used by the developer.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    public void debug(Object msg);

    /**
     * Allows for an error message to be displayed. This method will not handle
     * the error in itself and must still be handled by the calling class.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    public void err(Object msg);
}