package io;

import io.out.Simple;

/**
 * This class is the implementation of the Input/Output class which should
 * allow for messages to be displayed to the user. It will also give input
 * methods in the future for the implementing class to handle and the Game to
 * use as a method of interaction from the user.
 * <p>
 * @author dan
 * @version 1.0
 */
public class Terminal implements InOut{
    private static Terminal instance;

    /**
     * A Terminal instance is only created if <code>System.console() != null</code>
     * Only allow one instance of Terminal, stops lots of bugs in the
     * future when two terminals are running at the same time, which
     * can mean the outputs can overwrite each other, which has
     * happened to me in the past
     * <p>
     * @return the instance of terminal
     */
    public static Terminal getInstance(){
        if(instance==null&&System.console()!=null)instance = new Terminal();
        return instance;
    }

    /**
     * Constructor for the terminal, which is current responsible for displaying
     * debug information about the terminal detected, turning on the debug
     * setting and telling the user that the debug mode has been activated.
     */
    private Terminal(){
        Simple.debug(true); //TODO: Make debug mode an optional setting
        Simple.status(Simple.State.OK, "Terminal started");
        //Displays debug information about the type of termial detected.
        Simple.status(Simple.State.DEBUG, "Linux " + (Simple.linux() ? "is" : "not") + " detected");
        Simple.status(Simple.State.DEBUG, "Windows " + (Simple.windows() ? "is" : "not") + " detected");
        Simple.status(Simple.State.DEBUG, "Mac " + (Simple.mac() ? "is" : "not") + " detected");
        Simple.status(Simple.State.DEBUG, "Unknown " + (Simple.unknown() ? "is" : "not") + " detected");
    }

    /**
     * Allows for a simple message to be displayed, making the user aware that
     * either something has complete or something significant that is also good
     * has happened.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    @Override
    public void message(String msg){ Simple.status(Simple.State.OK, msg); }

    /**
     * Allows a debug message to be displayed. By default this won't print until
     * it is turned on. This method should be typically used by the developer.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    @Override
    public void debug(String msg){ Simple.status(Simple.State.DEBUG, msg); }

    /**
     * Allows for an error message to be displayed. This method will not handle
     * the error in itself and must still be handled by the calling class.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    @Override
    public void err(String msg){ Simple.status(Simple.State.FAIL, msg); }

    /**
     * Allows for a simple message to be displayed, making the user aware that
     * either something has complete or something significant that is also good
     * has happened.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    @Override
    public void message(Object msg){ Simple.status(Simple.State.OK, msg.toString()); }

    /**
     * Allows a debug message to be displayed. By default this won't print until
     * it is turned on. This method should be typically used by the developer.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    @Override
    public void debug(Object msg){ Simple.status(Simple.State.DEBUG, msg.toString()); }

    /**
     * Allows for an error message to be displayed. This method will not handle
     * the error in itself and must still be handled by the calling class.
     * <p>
     * @param msg the message to be displayed with the status.
     */
    @Override
    public void err(Object msg){ Simple.status(Simple.State.FAIL, msg.toString()); }
}