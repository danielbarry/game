package io;

import entities.Entity;
import entities.mob.Player;
import game.map.Map;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import javax.swing.JPanel;
import sprites.Sprite;
import tiles.Tile;

/**
 *
 * @author Paul
 * @version 0.1 build 1 "soft version name"
 */
public class Draw extends JPanel {

    //Game references
    private Map map;
    private Entity[] entities;
    private Player player;

    //settings from GUI
    private int bightness;
    private int saturation;
    private int maxFPS;

    public Draw(Map map, Entity[] entities, Player player, int bightness, int saturation, int maxFPS, String qualityLevel) {
        this.map = map;
        this.entities = entities;
        this.player = player;
        this.bightness = bightness;
        this.saturation = saturation;
        this.maxFPS = maxFPS;
    }

    /**
     * loads all the images for the entities of the Game.
     *
     * @return HashMap containing BufferedImages referenced by Entities
     */
    private HashMap<String, BufferedImage> loadSrites() {
        //TODO make this Ollie code compatible!
        HashMap<String, BufferedImage> allSprites = new HashMap<String, BufferedImage>();
        for (Tile tile : map.getMap()) {
            //load in sprites
        }
        for (Entity entity : entities) {
            //load in sprites
        }
        
        return null;
    }



    /**
     * Modifies the brightness
     *
     * @param brightness int (-255 is dark) (0 is no change) (255 is light) to
     * apply
     * <p>
     * @return the modified image as int[]
     */
    private int[] brightnessFilter(int[] imageIntArray, int brightness) {
        if (brightness != 0) {
            for (int i = 0; i < imageIntArray.length; i++) {
                int pixel = imageIntArray[i];
                if (pixel == Sprite.getTransparentColour()) {
                    //if the pixel is the transparent colour
                    //go to the next pixel
                    continue;
                }
                int red = (pixel >> 16) & 255;
                int green = (pixel >> 8) & 255;
                int blue = pixel & 255;

                //add the brightness to the colours
                //converting float to int doesn't affect the 
                red = (int) constrain(red + brightness, 0, 255);
                green = (int) constrain(green + brightness, 0, 255);
                blue = (int) constrain(blue + brightness, 0, 255);

                //
                int rgb = red;
                rgb = (rgb << 8) + green;
                rgb = (rgb << 16) + blue;
                imageIntArray[i] = rgb;
            }
        }
        return imageIntArray;
    }

    /**
     * Modifies the saturation
     * <p>
     * @param imageIntArray image to modify
     * @param saturationLv saturation level to apply ( 0 is no change) (-100 is
     * grey) (100 is super-amazing colour!)
     * @return modified image
     */
    public int[] saturationFilter(int[] imageIntArray, int saturationLv) {
        if (saturationLv != 0) {
            for (int i = 0; i < imageIntArray.length; i++) {
                int pixel = imageIntArray[i];
                if (pixel == Sprite.getTransparentColour()) {
                    //if the pixel is the transparent colour
                    //go to the next cheeky chimp
                    continue;
                }
                int red = pixel & 255;
                int green = (pixel >> 8) & 255;
                int blue = (pixel >> 16) & 255;

                float[] hsv = new float[3];
                Color.RGBtoHSB(red, green, blue, hsv);

                hsv[1] = hsv[1] + ((float) (saturationLv * 0.01)); //adjust the saturation

                //if it overflows
                hsv[1] = constrain(hsv[1], 0, 1);

                int newpixel = Color.HSBtoRGB(hsv[0], hsv[1], hsv[2]);
                imageIntArray[i] = newpixel;
            }
        }
        return imageIntArray;
    }

    /**
     * Constrains a value between the <code>min</code> and <code>max</code>
     * values if the value is less than <code>min</code>, it is returned as
     * <code>min</code> if the value is greater than <code>max</code>, it is
     * returned as <code>max</code>
     * <p>
     * @param value the value to constrain
     * @param min the min value allowed
     * @param max the max value allowed
     * <p>
     * @return the constrained value
     */
    private float constrain(float value, float min, float max) {
        if (min > max) {
            float temp = min;
            min = max;
            max = temp;
        }
        if (value < min) {
            return min;
        } else if (value > max) {
            return max;
        }
        return value;
    }

    /**
     * This method should be called if the window size is changed, in doing so
     * the graphic are re-scaled and re-loaded into there respect HashMaps
     */
    public void reload() {
        if (super.getWidth() > 0 && super.getHeight() > 0) {
            //reload images

            //TODO re-scale HUD
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.BLACK);

        //HUD
        g.setColor(Color.GREEN);
        g.drawString("Score: " + player.getScore(), 10, 15);
        if (player.getActiveWeapon() != null) {
            g.drawString("Weapon: " + player.getActiveWeapon().getName(), 10, 30);
        } else {
            g.drawString("Weapon: Unarmed", 10, 30);
        }
        g.drawString("Health: " + player.getHealth() + "/" + player.getMaxHealth(), 10, 45);
        if (player.getHealth() > 0) {
            g.setColor(new Color(255 - ((player.getHealth() / player.getMaxHealth()) * 255), ((player.getHealth() / player.getMaxHealth()) * 255), 0)); //between red and green
            g.fillRect(10, 50, 100, 5); //draw Health bar
        }

    }
}
