package io;

/**
 *
 * @author Paul
 * @version 0.1 build 1 "soft version name"
 */
public enum MaxUpdatesPerSecond {

    PAINFUL(5,"Painful"),
    SLOW(10,"Slow"),
    NORMAL(20,"Normal"),
    FAST(30,"Fast"),
    ULTRA(60,"Ultra");
    
    private final Integer UPSrate;
    private final String name;
    
    MaxUpdatesPerSecond(int UPSrate, String name)
    {
        this.UPSrate = UPSrate;
        this.name = name;
    }
            
    public int getUPSRate()
    {
        return UPSrate;
    }
    
    @Override
    public String toString()
    {
        return name;
    }
}
