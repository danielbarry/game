package tiles;

import sprites.Sprite;
import utils.math.Position;

/**
 *
 * @author Oliver
 */
public class Tile{

    private Position p;
    private boolean solid;
    private Sprite sprite;


    /**
     * Creates a new tile, it it given a position, a sprite and it is set to either solid or not
     * <p>
     * @param isSolid <code>true</code> if the tile is solid (eg. a wall)
     * @param p       the position of the tile in the game
     * @param sprite  the sprite the tile is
     */
    public Tile(boolean isSolid, Position p, Sprite sprite){
        this.p = p;
        this.sprite = sprite;
        this.solid = isSolid;
    }


    /**
     * Creates a new tile, it is not solid, with the given position and sprite
     * <p>
     * @param p      the position of the tile in the game
     * @param sprite the sprite the tile has
     */
    public Tile(Position p, Sprite sprite){
        this(false, p, sprite);
    }


    public void setPosition(Position p){
        this.p = p;
    }


    /**
     * returns the position of the tile
     * <p>
     * @return the position of the tile
     */
    public Position getPosition(){
        return p;
    }


    /**
     * returns the sprite
     * <p>
     * @return the sprite
     */
    public Sprite getSprite(){
        return sprite;
    }


    /**
     * <code>true</code> if the tile is solid
     * <p>
     * @return <code>true</code> if the tile is solid
     */
    public boolean isSolid(){
        return solid;
    }
;
}
