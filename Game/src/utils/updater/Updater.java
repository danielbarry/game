package utils.updater;

import game.Game;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.swing.SwingUtilities;

public class Updater{
    private UpdaterGUI out = null;
    private boolean development = !(new File("Game.jar")).exists();
    private String tempPath = development ? "./dist/Temp.jar" : "Temp.jar";
    private String oldPath = development ? "./dist/Game.jar" : "Game.jar";
    
    public static void main(String[] args){ new Updater(); }
    
    public Updater(){
        out = new UpdaterGUI("Updater");
        SwingUtilities.invokeLater(() -> {
            out.setVisible(true);
        });
        if(Game.jHnd.getBoolean("application.update.check")){
            if(out!=null)out.message("Looking for update");
            int major = Game.jHnd.getInt("application.version.major");
            int minor = Game.jHnd.getInt("application.version.minor");
            int patch = Game.jHnd.getInt("application.version.patch");

            String downloadPath = checkForVersion(major, minor, patch);
            if(downloadPath==null)if(out!=null)out.err("Could not check current version");
            if(checkForVersion(major + 1, 0, 0)!=null){
                //Start update
                if(downloadUpdate(downloadPath))replaceAndRun();
                else if(out!=null)out.message("Unable to update");
            }else{
                downloadPath = checkForVersion(major, minor + 1, 0);
                if(downloadPath!=null){
                    //Start update
                    if(downloadUpdate(downloadPath))replaceAndRun();
                    else if(out!=null)out.message("Unable to update");
                }else{
                    downloadPath = checkForVersion(major, minor, patch + 1);
                    if(downloadPath!=null){
                        //Start update
                        if(downloadUpdate(downloadPath))replaceAndRun();
                        else if(out!=null)out.message("Unable to update");
                    }else if(out!=null)out.message("No update available");
                }
            }
        }else if(out!=null)out.message("Update disabled");
        out.dispose();
        out.end();
    }
    
    private String checkForVersion(int x, int y, int z){
        out.progress(out.getProgress() + 10);
        try{
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection huc = (HttpURLConnection)(new URL(Game.jHnd.getString("application.update.host.location") + x + '.' + y + '.' + z + ".jar").openConnection());
            huc.setRequestMethod("HEAD");

            while(huc.getResponseCode()==HttpURLConnection.HTTP_MOVED_PERM||huc.getResponseCode()==HttpURLConnection.HTTP_MOVED_TEMP){
                huc = (HttpURLConnection)(new URL(huc.getHeaderField("Location")).openConnection());
            }

            return huc.getURL().getHost().equals(Game.jHnd.getString("application.update.host.url")) ? null : huc.getURL().toString();
        }catch(IOException e){ return null; }
    }
    
    private boolean downloadUpdate(String downloadPath){
        if(out.ask("Do you wish to update?")){
            out.progress(out.getProgress() + 20);
            if(out!=null)out.message("Downloading new file");
            try{
                HttpURLConnection.setFollowRedirects(false);
                HttpURLConnection huc = (HttpURLConnection)(new URL(downloadPath).openConnection());
                huc.setRequestMethod("GET");

                byte[] buffer = new byte[8 * 1024];

                try(InputStream input = huc.getInputStream(); OutputStream output = new FileOutputStream(tempPath)){
                    int bytesRead;
                    while((bytesRead = input.read(buffer))!=-1){
                        output.write(buffer, 0, bytesRead);
                    }
                    output.close();
                    input.close();
                    return true;
                }
            }catch(IOException e){ return false; }
        }else return false;
    }
    
    private void replaceAndRun(){
        if(out!=null)out.message("Deleting old version");
        out.progress(out.getProgress() + 10);
        
        File old = new File(oldPath);
        if(!old.exists())if(out!=null)out.err("Old file not found");
        old.delete();
        (new File(tempPath)).renameTo(old);
        
        if(out!=null)out.message("Running new version");
        out.progress(100);
        
        Process p = null;
        try{ p = Runtime.getRuntime().exec("java -jar " + oldPath); }
        catch(IOException e){ if(out!=null)out.err(e.toString()); }
        
        out.dispose();
        out.end();
        
        if(p!=null){
            try{ p.waitFor(); }
            catch(InterruptedException e){ if(out!=null)out.err("Failed to wait for new process to end"); }
        }else if(out!=null)out.err("Failed to create new process");
        
        if(out!=null)out.message("System going into shutdown");
        
        System.exit(0);
    }
    
    public static String getVersion(){ return "Version: " + Game.jHnd.getInt("application.version.major") + '.' + Game.jHnd.getInt("application.version.minor") + '.' + Game.jHnd.getInt("application.version.patch"); }
}