package utils.updater;

import game.Game;
import io.InOut;
import io.Terminal;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

public class UpdaterGUI extends JFrame implements InOut, ActionListener{
    public class JPanelWithBackground extends JPanel{
        private final Image backgroundImage;
        private int width = 0, height = 0;

        public JPanelWithBackground(URL fileName) throws IOException{ backgroundImage = ImageIO.read(fileName); }
        
        public void setBackgrondSize(int width, int height){
            this.width = width;
            this.height = height;
        }

        @Override
        public void paintComponent(Graphics g){
            super.paintComponent(g);
            g.drawImage(backgroundImage, 0, 0, width, height, this);
        }
    }
    
    private static final int WIDTH = 600, HEIGHT = 200;
    private Terminal term = null;
    private boolean ended = false;
    private final HashMap<String, Component> comps = new HashMap<String, Component>();
    private int progress = 0;
    
    public UpdaterGUI(String title){
        //Add title to GUI title
        super(title);
        
        //Setup Terminal if console exists
        term = Terminal.getInstance();
        
        //Set image icon
        try{
            InputStream iconStream = getClass().getResourceAsStream(Game.jHnd.getString("application.icon"));
            setIconImage(ImageIO.read(iconStream));
        }catch(IOException e){ err("Could not load Image Icon"); }
        
        //Setup simple window
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(WIDTH, HEIGHT);
        setLocationRelativeTo(null); //NOTE: Makes window centered
        setResizable(false);
        setLayout(new BorderLayout());
        
        //Create components for GUI
        
        //Title
        JLabel titleLabel = new JLabel(Game.jHnd.getString("application.name") + " Updater -> " + Updater.getVersion());
        titleLabel.setFont(new Font("Verdana", Font.BOLD, 16));
        titleLabel.setBackground(Color.BLACK);
        titleLabel.setForeground(Color.DARK_GRAY);
        titleLabel.setOpaque(true);
        titleLabel.repaint();
        comps.put("title", titleLabel);
        add(titleLabel, BorderLayout.NORTH);
        
        //Lower Screen
        comps.put("bottom", new Container());
        add(comps.get("bottom"), BorderLayout.SOUTH);
        ((Container)comps.get("bottom")).setLayout(new GridLayout(2, 1));
        
        comps.put("progress", new JProgressBar(progress, 100){
            @Override
            public void paint(Graphics g){
                if(!ended&&comps.get("progress")!=null)updateProgress();
                super.paint(g);
            }
        });
        ((Container)comps.get("bottom")).add(comps.get("progress"));
        
        comps.put("status", new JLabel("Starting Update GUI..."));
        ((Container)comps.get("bottom")).add(comps.get("status"));
        
        //Load background image if possible
        URL imagePath = null;
        try{ imagePath = new URL(getImage(Game.jHnd.getString("application.update.image.location"))); }
        catch(MalformedURLException e){ debug("Failed to find url"); }
        debug(imagePath);
        JPanelWithBackground jpwb = null;
        try{ jpwb = new JPanelWithBackground(imagePath); }
        catch(IOException e){ debug("Failed to load image"); }
        jpwb.setBackgrondSize(WIDTH, HEIGHT);
        if(jpwb!=null)getContentPane().add(jpwb);
        else debug("Cannot set background image");
    }
    
    private String getImage(String urlPath){
        URL url = null;
        try{ url = new URL(urlPath); }
        catch(MalformedURLException e){ debug("Failed to find url"); }
        URLConnection con = null;
        try{ con = url.openConnection(); }
        catch(IOException e){ debug(e); }
        
        //Update progress
        progress(getProgress() + 10);
        
        if(con!=null){
            con.setReadTimeout(2000);
            Pattern p = Pattern.compile("text/html;\\s+charset=([^\\s]+)\\s*");
            Matcher m = p.matcher(con.getContentType());
            /* If Content-Type doesn't match this pre-conception, choose default and 
             * hope for the best. */
            String charset = m.matches() ? m.group(1) : "ISO-8859-1";
            Reader r = null;
            try{ r = new InputStreamReader(con.getInputStream(), charset); }
            catch(IOException e){ debug(e); }
            StringBuilder buf = new StringBuilder();
            while(true){
                int ch = 0;
                try{ ch = r.read(); }
                catch(IOException e){}
                if(ch<0)break;
                buf.append((char)ch);
            }
            String str = buf.toString();

            int start = str.toLowerCase().indexOf("img src=\"") + 9;
            int end = str.indexOf("\"", start);

            return Game.jHnd.getString("application.update.image.archive") + str.substring(start, end);
        }else return "NOT FOUND";
    }
    
    public void progress(int progress){ this.progress = progress; }
        
    private void updateProgress(){ if(!ended)((JProgressBar)comps.get("progress")).setValue(progress); }
    
    public int getProgress(){ return progress; }

    @Override
    public void message(String msg){
        if(term!=null)term.message(msg);
        if(!ended)((JLabel)comps.get("status")).setText(msg + "...");
    }

    @Override
    public void debug(String msg){
        if(term!=null)term.debug(msg);
        if(!ended)((JLabel)comps.get("status")).setText("Debug>>" + msg + "...");
    }

    @Override
    public void err(String msg){
        if(term!=null)term.err(msg);
        if(!ended)JOptionPane.showMessageDialog(this, msg, "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    public boolean ask(String msg){
        if(!ended){
            debug("Asking user to update");
            //Handle ask scenario
            return JOptionPane.showConfirmDialog(this, msg, "Option", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION;
        }
        return false;
    }
    
    @Override
    public void message(Object obj){
        if(term!=null)term.message(obj.toString());
        if(!ended)((JLabel)comps.get("status")).setText(obj.toString() + "...");
    }

    @Override
    public void debug(Object obj){
        if(term!=null)term.debug(obj.toString());
        if(!ended)((JLabel)comps.get("status")).setText("Debug>>" + obj.toString() + "...");
    }

    @Override
    public void err(Object obj){
        if(term!=null)term.err(obj.toString());
        if(!ended)JOptionPane.showMessageDialog(this, obj.toString(), "Error", JOptionPane.ERROR_MESSAGE);
    }
    
    @Override
    public void actionPerformed(ActionEvent e){}
    
    public void end(){ ended = true; }
}