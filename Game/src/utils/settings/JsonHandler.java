// TODO: Do the following to complete the changes on this file
//         # Make updated to stored settings
//         - Ability to write settings
//         - Make checking of settings file an option
//         - Move this class into the JSON package
//         - Optimize speed of update

// Package
package utils.settings;

// Imports
import game.Game;
import io.Terminal;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.WeakHashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * <b>JsonHandler.java</b>
 * <br><br>
 * Written by B[]Array
 * <br>
 * Date: 11/08/2014
 * <br><br>
 * This class handles JSON settings, where it should be used as a light but
 * richly featured method of maintaining settings files. This class is not an
 * official part of the JSON library.
 * <br><br>
 * <u>Features:</u>
 * <ul>
 *   <li>Read value by specifying String path only</li>
 *   <li>Optimised reading of values when already read</li>
 *   <li>Flags when the settings file has been changed</li>
 * </ul>
 * <br><br>
 * <i>Modifications</i>
 * <ul>
 *   <li>11/08/2014 - B[]Array - Created JSON Handler</li>
 * </ul>
 */
public class JsonHandler{
    // The location of the original settings file
    private final static String SETTINGS_PATH = "/assets/settings/";
    // A WeakHashMap of all of the Objects created
    private final WeakHashMap<String, WeakReference<Object>> objects = new WeakHashMap<>();
    // Wait a second per look
    private int REFRESH_WAIT = 1000;
    // A HashMap of all of the paths (JSONObjects) we create
    private HashMap<String, JSONObject> paths = new HashMap<>();
    // Our main JSON Object
    private JSONObject jObj;
    // Our settings file
    private File settings = null;
    // Our settings InputStream
    private InputStream is;
    
    // Reliably test whether we are in developement mode (testing in debug or in
    // final release)
    private static final boolean development = !(new File(
        new java.io.File(JsonHandler.class.getProtectionDomain()
        .getCodeSource().getLocation().getPath()).getName())).exists();
    
    public JsonHandler(String filename){
        // TODO: Some where in our program we need to close this file and not
        // rely on auto Java closing (we might lock somebodies file in serious
        // cases)
        // Try to open the settings file
        try{ settings = new File(filename); }
        catch(NullPointerException e){}
        // If file does not exist (is still null), create it
        if(!settings.exists()){
            is = Game.class.getResourceAsStream(SETTINGS_PATH + filename);
            OutputStream fos = null;
            try{ fos = new FileOutputStream(settings); }
            catch(FileNotFoundException e){ Terminal.getInstance().err(e.toString()); }
            byte[] data = new byte [4096];
            try{
                // Read from original file and dump output to new settings file
                while(is.read(data)>0)fos.write(data);
                // Be considerate and close our output file
                fos.close();
            }catch(IOException e){ Terminal.getInstance().err("Could not load settings file"); }
            // Close the old input file
            try{ is.close(); }
            catch(IOException e){}
        }
        // Print some debug information for developers about what we just did
        if(Terminal.getInstance()!=null)Terminal.getInstance().debug("json -> " + settings.isFile());
        if(Terminal.getInstance()!=null)Terminal.getInstance().debug("json -> " + settings.getPath());
        // Open external settings file
        try{ is = new FileInputStream(settings); }
        catch(FileNotFoundException e){}
        // Use our settings file to create a token
        JSONTokener jtoke = new JSONTokener(is);
        // Create a JSON Object using the token
        jObj = new JSONObject(jtoke);
        // Test if settings file has been changed with non-blocking thread
        new Thread(new Runnable(){
            @Override
            public void run(){
                // See whether we should do constant checking or not
                if(getBoolean("application.settings.check")){
                    // Setup our intial knowledge of when the settings file was last
                    // modified
                    long lastM = settings.lastModified();
                    long newM = lastM;
                    // Infinite loop with thread wait allowing repeated disk check
                    // for changes. Objects are to be updated if they have a
                    // refference.
                    for(;;){
                        // Update stored refresh wait
                        REFRESH_WAIT = getInt("application.settings.refresh-time");
                        if(Terminal.getInstance()!=null)Terminal.getInstance().debug("json -> check settings file");
                        // Update the timestamp for when the file was last modified
                        lastM = newM;
                        // Wait for the selected time as not to cause disk thrashing
                        // and damage to the disk. If we wake up too early it's not
                        // the end of the world
                        try{ Thread.sleep(REFRESH_WAIT); }
                        catch(InterruptedException e){}
                        // Update our estimate of when the settings file was last
                        // modified
                        newM = settings.lastModified();
                        // Check whether the file was modified (the modified date
                        // would have changed considerably)
                        if(lastM!=newM){
                            if(Terminal.getInstance()!=null)Terminal.getInstance().debug("json -> updating because file changed");
                            // Close our InputSream
                            try{ is.close(); }
                            catch(IOException e){
                                Terminal.getInstance().err("Failed to close settings file");
                                break; // Catch it on the next loop
                            }
                            // Reload our settings file object
                            try{ settings = new File(filename); }
                            catch(NullPointerException e){
                                Terminal.getInstance().err("Could not find filename");
                                break; // Catch it on the next loop
                            }
                            // Update our file in our tree structure
                            try{ is = new FileInputStream(settings); }
                            catch(FileNotFoundException e){
                                Terminal.getInstance().err("Failed to open settings file");
                                break; // Catch it on the next loop
                            }
                            // Use our settings file to create a token
                            JSONTokener jtoke = new JSONTokener(is);
                            // Create a JSON Object using the token
                            jObj = new JSONObject(jtoke);
                            // Create a temporary copy of our old Map
                            WeakHashMap<String, WeakReference<Object>> tempObjects = new WeakHashMap<String, WeakReference<Object>>(){{
                                objects.forEach((key, val) -> { this.put(key, val); });
                            }};
                            // Reset all of our paths
                            paths = new HashMap<>();
                            // Update each item in the array so that the strong
                            // reference gets updated
                            tempObjects.forEach((key, val) -> {
                                // NOTE: As we are using a WeakHashMap, values thats
                                //       are not in use are automatically removed.
                                Object obj = val.get();
                                // Load key value
                                get(key, true);
                                if(Terminal.getInstance()!=null)Terminal.getInstance().debug("json -> " + objects.get(key).get().toString());
                                // TODO: Replace old objects here
                            });
                            // Update when the file was last modified
                            lastM = newM;
                        }
                    }
                }
            }
        }).start();
    }
    
    private JSONObject get(String key, boolean forceObjectUpdate){
        // Split a String up so that each part is treated seperately
        String path[] = key.split("\\.");
        // Store the top of our tree
        JSONObject last = jObj;
        // Our current path on the tree
        String current = "";
        // Itterate over our broken String
        for(int p=0; p<path.length-1; p++){
            // Add back in our splits as not to confuse the indexing
            // NOTE: This resolves the issue where a path could be
            //         "help.me" or "helpme", where only the dot makes them
            //         unique but both are valid.
            current += '.' + path[p];
            // If the current key hasn't been added, then add it
            if(!paths.containsKey(current))paths.put(current, last.getJSONObject(path[p]));
            // Our last position on the tree is where we are at now
            last = paths.get(current);
        }
        
        // The last place is always where we were trying to get to and therefore
        // the place of interest
        
        // If target Object doesn't exist in our weak reference then add it
        if(objects.get(key) == null || forceObjectUpdate){
            objects.remove(key);
            objects.put(key, new WeakReference(last.get(getField(key))));
        }
        
        // Return the last object
        return last;
    }
    
    private String getField(String key){ return key.substring(key.lastIndexOf('.') + 1); }
    
    public Object getObject(String key){
        get(key, false);
        return (Object)objects.get(key).get();
    }
    
    public Boolean getBoolean(String key){
        get(key, false);
        return (Boolean)objects.get(key).get();
    }
    
    public Double getDouble(String key){
        get(key, false);
        return (Double)objects.get(key).get();
    }
    
    public Integer getInt(String key){
        get(key, false);
        return (Integer)objects.get(key).get();
    }
    
    public JSONArray getJSONArray(String key){
        get(key, false);
        return (JSONArray)objects.get(key).get();
    }
    
    public JSONObject getJSONObject(String key){
        get(key, false);
        return (JSONObject)objects.get(key).get();
    }
    
    public Long getLong(String key){
        get(key, false);
        // Have to cast to Integer before casting to Long, because apparently an
        // Integer cannot be cast to a Long. If you know of a better way of
        // doing this please replace it.
        return new Long((Integer)objects.get(key).get());
    }
    
    public String getString(String key){
        get(key, false);
        return (String)objects.get(key).get();
    }
}