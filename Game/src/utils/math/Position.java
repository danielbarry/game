package utils.math;

/**
 *
 * @author Oliver
 */
public class Position{

    private int x, y;
    private double rotation;


    /**
     * Creates a new vector storing the x, y and rotation values
     * <p>
     * @param x        the x position on the grid
     * @param y        the y position on the grid
     * @param rotation the rotation of the entity, in radians
     *                 0° is facing right
     */
    public Position(int x, int y, double rotation){
        this.x = x;
        this.y = y;
        this.rotation = rotation;
    }


    /**
     * Gets the x position
     * <p>
     * @return the x position
     */
    public int getX(){
        return x;
    }


    /**
     * Gets the y position
     * <p>
     * @return the y position
     */
    public int getY(){
        return y;
    }


    /**
     * Gets the rotation
     * <p>
     * @return the rotation
     */
    public double getRotation(){
        return rotation;
    }


    /**
     * Sets the x position to a new x
     * <p>
     * @param x the new x
     */
    public void setX(int x){
        this.x = x;
    }


    /**
     * Sets the y position to a new y
     * <p>
     * @param y the new y
     */
    public void setY(int y){
        this.y = y;
    }


    /**
     * Sets both the x and y positions to new x and y positions
     * <p>
     * @param x the new x
     * @param y the new y
     */
    public void setXY(int x, int y){
        setX(x);
        setY(y);
    }


    /**
     * Sets the rotation to a new rotation
     * <p>
     * @param rotation the new rotation
     */
    public void setRotation(double rotation){
        this.rotation = rotation;
    }


    /**
     * Adds x to the current x position
     * <p>
     * @param x the value to add
     */
    public void addX(int x){
        this.x += x;
    }


    /**
     * Adds y to the current y position
     * <p>
     * @param y the value to add
     */
    public void addY(int y){
        this.y += y;
    }


    /**
     * Adds an x and y value to the current x and y values
     * <p>
     * @param x the x value to add
     * @param y the y value to add
     */
    public void addXY(int x, int y){
        addX(x);
        addY(y);
    }


    /**
     * Adds a rotation to the current rotation
     * <p>
     * @param rotation the rotation to add
     */
    public void addRotaion(double rotation){
        this.rotation += rotation;
    }

    
    @Override
    public String toString(){
        return "x: " + x + " y: " + y + " rot: " + rotation;
    }
}
