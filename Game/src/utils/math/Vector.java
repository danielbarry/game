package utils.math;

public class Vector<E>{
    //---- Constants ----
    
    private enum NumberType{
        BYTE, DOUBLE, FLOAT, INTEGER, LONG, SHORT, UNKNOWN;
    }
    
    private enum MathType{
        ADD, SUB, DIV, MUL;
    }
    
    //---- Variables ----
    
    private Object[] values = null;
    
    //---- Constructors ----
    
    public Vector(){ values = new Object[0]; }
    
    public Vector(E e1){ values = new Object[]{e1}; }
    
    public Vector(E e1, E e2){ values = new Object[]{e1, e2}; }
    
    public Vector(E e1, E e2, E e3){ values = new Object[]{e1, e2, e3}; }
    
    public Vector(E[] en){ values = new Object[]{en}; }
    
    public Vector(Vector<E> vector){
        values = new Object[vector.length()];
        for(int i=0; i<length(); i++){
            values[i] = vector.get(i);
        }
    }
    
    //---- Accessors ----
    
    public int length(){ return values.length; }
    
    public E getX(){
        if(length()>0)return (E)values[0];
        else throw new ArrayIndexOutOfBoundsException("X does not exist");
    }
    
    public E getY(){
        if(length()>1)return (E)values[1];
        else throw new ArrayIndexOutOfBoundsException("Y does not exist");
    }
    
    public E getZ(){
        if(length()>2)return (E)values[2];
        else throw new ArrayIndexOutOfBoundsException("Z does not exist");
    }
    
    public E get(int n){
        if(length()>=n)return (E)values[n];
        else throw new ArrayIndexOutOfBoundsException(n + " position does not exist");
    }
    
    public E[] getValues(){ return (E[])values; }
    
    public boolean equals(Vector<E> that){
        if(length()>0){
            if(length()!=that.length())return false;
            for(int i=0; i<length(); i++)if(values[i] != that.get(i))return false;
        }
        return true;
    }
    
    public Vector distance(Vector<E> that){
        if(length()!=that.length())throw new VectorMismatchException("Vector lengths are not of equal length");
        Vector<Double> distance = new Vector<Double>(0d);
        Vector<E> vector = new Vector<E>(this);
        vector = vector.sub(that);
        for(E val : vector.getValues())distance.setX(distance.getX() + (Math.pow(((Number)val).doubleValue(), 2d)));
        distance.setX(Math.pow(distance.getX(), 0.5d));
        return distance;
    }
    
    //---- Mutators ----
    
    public void setX(E e1){
        if(length()>0)values[0] = e1;
        else throw new ArrayIndexOutOfBoundsException("X does not exist");
    }
    
    public void setY(E e2){
        if(length()>1)values[0] = e2;
        else throw new ArrayIndexOutOfBoundsException("Y does not exist");
    }
    
    public void setZ(E e3){
        if(length()>2)values[0] = e3;
        else throw new ArrayIndexOutOfBoundsException("Z does not exist");
    }
    
    public void set(int n, E en){
        if(length()>=n)values[n] = en;
        else throw new ArrayIndexOutOfBoundsException(n + " position does not exist");
    }
    
    public void set(Vector<E> vector){
        if(length()!=vector.length())throw new VectorMismatchException("Vector lengths are not of equal length");
        values = new Object[vector.length()];
        for(int i=0; i<length(); i++){
            values[i] = vector.get(i);
        }
    }
    
    private Vector<E> getResult(MathType math, Vector<E> that) throws VectorNotNumberException, VectorMismatchException{
            if(length()>0){
            if(!((E)values[0] instanceof Number))throw new VectorNotNumberException("Vector is not of Number type");
            if(length()!=that.length())throw new VectorMismatchException("Vector lengths are not of equal length");
            Vector<E> vector = new Vector<E>(this);
            NumberType type = vector.getTypeNumberClass();
            for(int i=0; i<vector.length(); i++)vector.set(i, (E)vector.getResult(math, type, (Number)vector.get(i), (Number)that.get(i)));
            return vector;
        }
        return null;
    }
    
    public Vector<E> add(Vector<E> that) throws VectorNotNumberException, VectorMismatchException{ return getResult(MathType.ADD, that); }
    
    public Vector<E> sub(Vector<E> that) throws VectorNotNumberException, VectorMismatchException{ return getResult(MathType.SUB, that); }
    
    public Vector<E> div(Vector<E> that) throws VectorNotNumberException, VectorMismatchException{ return getResult(MathType.DIV, that); }
    
    public Vector<E> mul(Vector<E> that) throws VectorNotNumberException, VectorMismatchException{ return getResult(MathType.MUL, that); }
    
    private Number getResult(MathType math, NumberType type, Number n1, Number n2){
        switch(math){
            case ADD  : switch(type){
                            case BYTE    : return n1.byteValue() + n2.byteValue();
                            case DOUBLE  : return n1.doubleValue() + n2.doubleValue();
                            case FLOAT   : return n1.floatValue() + n2.floatValue();
                            case INTEGER : return n1.intValue() + n2.intValue();
                            case LONG    : return n1.longValue() + n2.longValue();
                            case SHORT   : return n1.shortValue() + n2.shortValue();
                            default      : return null;
                        }
            case SUB  : switch(type){
                            case BYTE    : return n1.byteValue() - n2.byteValue();
                            case DOUBLE  : return n1.doubleValue() - n2.doubleValue();
                            case FLOAT   : return n1.floatValue() - n2.floatValue();
                            case INTEGER : return n1.intValue() - n2.intValue();
                            case LONG    : return n1.longValue() - n2.longValue();
                            case SHORT   : return n1.shortValue() - n2.shortValue();
                            default      : return null;
                        }
            case DIV  : switch(type){
                            case BYTE    : return n1.byteValue() / n2.byteValue();
                            case DOUBLE  : return n1.doubleValue() / n2.doubleValue();
                            case FLOAT   : return n1.floatValue() / n2.floatValue();
                            case INTEGER : return n1.intValue() / n2.intValue();
                            case LONG    : return n1.longValue() / n2.longValue();
                            case SHORT   : return n1.shortValue() / n2.shortValue();
                            default      : return null;
                        }
            case MUL  : switch(type){
                            case BYTE    : return n1.byteValue() * n2.byteValue();
                            case DOUBLE  : return n1.doubleValue() * n2.doubleValue();
                            case FLOAT   : return n1.floatValue() * n2.floatValue();
                            case INTEGER : return n1.intValue() * n2.intValue();
                            case LONG    : return n1.longValue() * n2.longValue();
                            case SHORT   : return n1.shortValue() * n2.shortValue();
                            default      : return null;
                        }
            default   : return null;
        }
    }
    
    private NumberType getTypeNumberClass(){
        Object obj = (E)(new Integer(0));
        if(obj.getClass() == Byte.class)return NumberType.BYTE;
        if(obj.getClass() == Double.class)return NumberType.DOUBLE;
        if(obj.getClass() == Float.class)return NumberType.FLOAT;
        if(obj.getClass() == Integer.class)return NumberType.INTEGER;
        if(obj.getClass() == Long.class)return NumberType.LONG;
        if(obj.getClass() == Short.class)return NumberType.SHORT;
        return NumberType.UNKNOWN;
    }
    
    //TODO: Complete rotation math (http://en.wikipedia.org/wiki/Rotation_%28mathematics%29)
    
    public Vector<E> rotateX(Double val){
        if(length()<1)throw new ArrayIndexOutOfBoundsException("X does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(0, val, new Vector<E>((E[])init));
    }
    
    public Vector<E> rotateY(Double val){
        if(length()<2)throw new ArrayIndexOutOfBoundsException("Y does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(0, val, new Vector<E>((E[])init));
    }
    
    public Vector<E> rotateZ(Double val){
        if(length()<3)throw new ArrayIndexOutOfBoundsException("Z does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(0, val, new Vector<E>((E[])init));
    }
    
    public Vector<E> rotateN(int n, Double val){
        if(length()<n)throw new ArrayIndexOutOfBoundsException(n + " position does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(n, val, new Vector<E>((E[])init));
    }
    
    public Vector<E> rotateX(Double val, Vector<E> center){
        if(length()<1)throw new ArrayIndexOutOfBoundsException("X does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(0, val, center);
    }
    
    public Vector<E> rotateY(Double val, Vector<E> center){
        if(length()<2)throw new ArrayIndexOutOfBoundsException("Y does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(0, val, center);
    }
    
    public Vector<E> rotateZ(Double val, Vector<E> center){
        if(length()<3)throw new ArrayIndexOutOfBoundsException("Z does not exist");
        Integer[] init = new Integer[length()];
        for(Integer i : init)i = 0;
        return rotateN(0, val, center);
    }
    
    public Vector<E> rotateN(int n, Double val, Vector<E> center){
        if(length()<n)throw new ArrayIndexOutOfBoundsException(n + " position does not exist");
        //NOTE: Only math that needs to be done
        return null;
    }
    
    //public Vector<E> rotate(Vector<E> vector){ return null; }
    
    //public Vector<E> rotate(Vector<E> vector, Vector<E> center){ return null; }
    
    public long getRotationsNo(){
        long rotations = 1;
        for(int i=2; i<=length(); i++)rotations *= i;
        return rotations / 2;
    }
    
    //---- Tester ----
    
    public static void main(String[] args){
        test(true, "tester started");
        
        Vector<Integer> v1 = new Vector<Integer>(1, 1, 1);
        test(v1 != null, "create v1");
        test(v1.length() == 3, "v1 length");
        
        int x = (int)v1.values[0];
        test(v1.getX() == x, "v1 get X");
        
        v1.setX(2);
        test(v1.getX() != x, "v1 change X");
        
        Vector<Integer> v3 = v1.add(new Vector<Integer>(2, 3, 3));
        test(v3.equals(new Vector<Integer>(4, 4, 4)), "v1 and v2 correctly added");
        
        Vector<Integer> v4 = v3.sub(new Vector<Integer>(2, 2, 2));
        test(v4.equals(new Vector<Integer>(2, 2, 2)), "v1 and v2 correctly minused");
        
        Vector<Integer> v5 = v4.div(new Vector<Integer>(2, 2, 2));
        test(v5.equals(new Vector<Integer>(1, 1, 1)), "v1 and v2 correctly divided");
        
        Vector<Integer> v6 = v5.mul(new Vector<Integer>(3, 3, 3));
        test(v6.equals(new Vector<Integer>(3, 3, 3)), "v1 and v2 correctly multiplyed");
        
        test(Math.round((Double)v4.distance(v3).getX()) == 3, "v3 and v4 distance");
        
        test(v1.getRotationsNo() == 3, "v1 has correct number of rotations");
        
        Vector<Integer> v7 = new Vector<Integer>(4, 4);
        test(v7.getRotationsNo() == 1, "v7 has correct number of rotations");
    }
    
    private static void test(boolean test, String msg){ out((test ? "[ OK ] " : "[FAIL] ") + msg); }
    
    private static void out(String msg){ System.out.println(msg); }
}