package utils.math;

public class VectorException extends RuntimeException{
    public VectorException(){}

    public VectorException(String msg){ super(msg); }

    public VectorException(Throwable cause){ super(cause); }

    public VectorException(String msg, Throwable cause){ super(msg, cause); }

    protected VectorException(String msg, Throwable cause, boolean enableSuppression, boolean writableStackTrace){ super(msg, cause, enableSuppression, writableStackTrace); }
}