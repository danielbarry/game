package utils.math;

public class VectorMismatchException extends VectorException{
    public VectorMismatchException(){}

    public VectorMismatchException(String msg){ super(msg); }

    public VectorMismatchException(Throwable cause){ super(cause); }

    public VectorMismatchException(String msg, Throwable cause){ super(msg, cause); }

    protected VectorMismatchException(String msg, Throwable cause, boolean enableSuppression, boolean writableStackTrace){ super(msg, cause, enableSuppression, writableStackTrace); }
}