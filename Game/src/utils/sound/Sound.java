package utils.sound;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

/**
 * All methods (including constructor) have no publicity modifier because
 * we don't want anyone creating a sound apart from the <code>SoundManager</code>
 * Without any modifier, the methods can only be seen from classes in the
 * same package as this one, which is slightly more restrictive than the
 * <code>protected</code> modifier
 * <p>
 * @author Oliver
 */
public class Sound{

    private final String name;
    private final AudioClip sound;


    /**
     * Creates a new sound with a name, and the path to the file
     * <p>
     * @param name the name to refer to the sound by
     * @param path the path to the file
     * <p>
     * @bug when a large wav file is given, the sound doesn't play
     */
    Sound(String name, String path){
        this.name = name;
        sound = Applet.newAudioClip(getURL(path));
    }


    /**
     * Gets the name of the sound
     * <p>
     * @return the name of the sound
     */
    String getName(){
        return name;
    }


    /**
     * Plays the sound once
     * starts in a new thread
     */
    void play(){
        new Thread(() -> sound.play()).start();
    }


    /**
     * Loops the sound infinitely
     * starts in a new thread
     */
    void loop(){
        new Thread(() -> sound.loop()).start();
    }


    /**
     * Stops the sound
     */
    void stop(){
        sound.stop();
    }


    /**
     * Gets the URL for the file given
     * <p>
     * @param fileName the file to find the URL of
     * <p>
     * @return the URL of the file
     */
    private URL getURL(String fileName){
        return getClass().getClassLoader().getResource(fileName);
    }
}
