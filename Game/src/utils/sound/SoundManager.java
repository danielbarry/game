package utils.sound;

import game.Game;
import java.util.HashMap;

/**
 * A class to allow simple interaction with all the sounds in the game
 * <p>
 * @author Oliver
 */
public class SoundManager{

    private static final String SOUND_JSON = "sounds.";
    private static final SoundManager INSTANCE = new SoundManager();


    /**
     * Gets the instance of <code>SoundManager</code> to be used in the game
     * only one <code>SoundManager</code> is allowed as while sounds are playing,
     * the game will not exit without being forced, and having
     * multiple instances of <code>SoundManager</code> will make it very
     * easy to miss a playing file, and there isn't any
     * advantage having more than one as all sounds are
     * played in a new thread anyway
     * <p>
     * @return the instance of <code>SoundManager</code>
     */
    public static SoundManager getInstance(){
        return INSTANCE;
    }

    private final HashMap<String, Sound> sounds = new HashMap<>();
    private final String path;


    /**
     * Creates a new SoundManager with the path given in the <code>settings.json</code> file
     */
    private SoundManager(){
        path = Game.jHnd.getString(SOUND_JSON + "path");
    }


    /**
     * Adds the sound defined in <code>settings.json</code> to the sound manager
     * <p>
     * @param name the name in <code>settings.json</code> for the sound
     */
    public void addSound(String name){
        String fileName = Game.jHnd.getString(SOUND_JSON + name);
        sounds.put(name, new Sound(name, path + fileName));
    }


    /**
     * Removes the sound defined in <code>settings.json</code> from the manager
     * <p>
     * @param name the name in <code>settings.json</code> for the sound
     */
    public void removeSound(String name){
        sounds.remove(name);
    }


    /**
     * Plays the sound defined in <code>settings.json</code> from the manager
     * <p>
     * @param name the name in <code>settings.json</code> for the sound
     */
    public void playSound(String name){
        if(sounds.containsKey(name)){
            sounds.get(name).play();
        }
    }


    /**
     * Loops the sound defined in <code>settings.json</code> from the manager
     * <p>
     * <code>stopSound(name)</code> or <code>stopAllSounds()</code> <b>MUST</b> be
     * be called at some point after this, as without it, the program will keep running
     * indefinitely
     * <p>
     * @param name the name in <code>settings.json</code> for the sound
     */
    public void loopSound(String name){
        if(sounds.containsKey(name)){
            sounds.get(name).loop();
        }
    }


    /**
     * Stops the sound defined in <code>settings.json</code> from the manager
     * <p>
     * @param name the name in <code>settings.json</code> for the sound
     */
    public void stopSound(String name){
        if(sounds.containsKey(name)){
            sounds.get(name).stop();
        }
    }


    /**
     * Stops all the sounds stored in the sound manager
     */
    public void stopAllSounds(){
        //stop all sounds at the same(effectively) time
        sounds.values().parallelStream().forEach((sound) -> sound.stop());
    }
}
