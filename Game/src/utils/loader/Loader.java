package utils.loader;

import io.InOut;
import java.io.IOException;
import java.util.Properties;

/**
 * @deprecated The class is no longer used due to being outdated by JSON
 */
public class Loader{
    //Constants
    private static final String SETTINGS_PATH = "/assets/settings/main.properties";

    //Variables
    private static Properties prop = new Properties();
    private static InOut io = null;

    static{
        try{ prop.load(Loader.class.getResourceAsStream(SETTINGS_PATH)); }
        catch(IOException | NullPointerException e){ if(io != null)io.err("Failed to open settings file"); }
    }

    public static void setInOut(InOut io){ Loader.io = io; }

    public static Object getSetting(String name){
        try{
            if(prop.containsKey(name)){
                Object obj = prop.get(name);
                if(io != null){
                    io.message("Found setting  '" + name + "' -> '" + obj.toString() + "'");
                }
                return obj;
            }
        }catch(NullPointerException e){}
        if(io != null)io.err("Failed to open setting '" + name + "'");
        return "";
    }


    public static boolean setSetting(String name, String value){
        //TODO: Search for failure cases
        prop.setProperty(name, value);
        return true;
    }
}
