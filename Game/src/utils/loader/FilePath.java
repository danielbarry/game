package utils.loader;

import java.io.File;

public abstract class FilePath{
    public static String getLocation(){ return new File(FilePath.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getPath(); }
}