package sprites;

import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import utils.loader.Loader;

/**
 *
 * @author Oliver
 */
public class SpriteSheet{

    private final String PATH;
    public final int SIZE;
    public int[] pixels;
    public static SpriteSheet tiles = new SpriteSheet((String)Loader.getSetting("tiles-sheet"), 256);
    public static SpriteSheet weapons = new SpriteSheet((String)Loader.getSetting("weapons-sheet"), 256);


    /**
     * Creates a new sprite sheet from the image path given, and of the size given
     * <p>
     * @param path the path of an image containing all the sprites
     * @param size the size of the image
     */
    public SpriteSheet(String path, int size){
        this.PATH = path;
        this.SIZE = size;
        pixels = new int[SIZE * SIZE];
        load();
    }


    /**
     * Loads the rgb values into the <code>pixel</code> array
     */
    private void load(){
        try{
            BufferedImage image = ImageIO.read(SpriteSheet.class.getResource(PATH));
            int w = image.getWidth();
            int h = image.getHeight();
            image.getRGB(0, 0, w, h, pixels, 0, w);
        }catch(IOException e){
            System.err.println(e);
        }
    }
}
