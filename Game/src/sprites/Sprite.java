package sprites;

/**
 *
 * @author Oliver
 */
public class Sprite{

    public final int SIZE;
    private int[] pixels;
    private SpriteSheet sheet;
    private int x, y;


    /**
     * Creates a new sprite from the given sprite sheet
     * <p>
     * @param size  the size in pixels of the sprite
     * @param x     the x position in the sprite sheet
     * @param y     the y position in the sprite sheet
     * @param sheet the sprite sheet that contains the sprite
     */
    public Sprite(int size, int x, int y, SpriteSheet sheet){
        this.SIZE = size;
        pixels = new int[SIZE * SIZE];
        this.x = x * SIZE;
        this.y = y * SIZE;
        this.sheet = sheet;
        load();
    }


    /**
     * Creates a sprite with just a solid colour
     * <p>
     * @param size   the size of the sprite
     * @param colour the colour of the sprite
     */
    public Sprite(int size, int colour){
        this.SIZE = size;
        pixels = new int[SIZE * SIZE];
        setColour(colour);
    }


    /**
     * Returns an array of <code>int</code>'s representing the <code>rgb</code>
     * colour of the sprite, the sprite is <code>SIZE</code> by <code>SIZE</code> big
     * <p>
     * @return an array of <code>rgb</code> values for the sprite
     */
    public int[] getPixels(){
        return pixels;
    }


    /**
     * Loads the pixels from the sprite sheet to the sprite
     */
    private void load(){
        for(int $y = 0; $y < SIZE; $y++){
            for(int $x = 0; $x < SIZE; $x++){
                pixels[$x + $y * SIZE] = sheet.pixels[($x + this.x) + ($y + this.y) * sheet.SIZE];
            }
        }
    }


    /**
     * Sets the sprite to be a solid colour
     * <p>
     * @param colour the colour to set the sprite to
     */
    public void setColour(int colour){
        for(int i = 0; i < SIZE * SIZE; i++){
            pixels[i] = colour;
        }
    }


    /**
     * Returns an int representing the colour of sprites which should be drawn as transparent
     * <p>
     * @return the transparent colour as an integer
     */
    public static int getTransparentColour(){
        return 0xFF00FF;
    }
}
