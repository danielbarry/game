package sprites;

import game.Game;
import io.Terminal;
import java.util.HashMap;
import org.json.JSONObject;

public class SpriteType{
    private enum Type{ TILE, WEAPON, OTHER; }
    
    private static final HashMap<String, Sprite> sprites = new HashMap<>();
    
    private String name;
    private Sprite sprite;
    private char mapChar;
    private Type spriteType;
    
    public SpriteType(JSONObject jObj){
        //NOTE: Get instance of terminal if it exists
        //only returns an instance if System.console() exists
        Terminal term = Terminal.getInstance();
        
        //NOTE: Get type
        String t = jObj.getString("type").toUpperCase();
        spriteType =Type.valueOf(t);
        
        //NOTE: Get other settings that may be dependant on type
        name = jObj.getString("name").toUpperCase();
        mapChar = (char)jObj.getInt("char");
        if(sprites.containsKey(name)&&term!=null)term.err("sprite already exists -> " + name);
        else{
            JSONObject jType = Game.jHnd.getJSONObject("tiles").getJSONObject("types").getJSONObject(spriteType.name()).getJSONObject("constants");
            //NOTE: Create sprite
            switch(spriteType){
                case TILE : sprites.put(name, new Sprite(jType.getInt("size"), jObj.getInt("x"), jObj.getInt("y"), getSheet(jType.getString("sheet"))));
                    break;
                case WEAPON : sprites.put(name, new Sprite(jType.getInt("size"), jObj.getInt("x"), jObj.getInt("y"), getSheet(jType.getString("sheet"))));
                    break;
                default : sprites.put(name, new Sprite(jType.getInt("size"), jObj.getInt("colour")));
            }
        }
        //NOTE: Add sprite
        sprite = sprites.get(name);
    }
    
    private SpriteSheet getSheet(String sheet){
        switch(sheet.toLowerCase()){
            case "tiles" : return SpriteSheet.tiles;
            case "weapons" : return SpriteSheet.weapons;
            default : return null;
        }
    }
    
    public Sprite getSprite(){ return sprite; }
    
    public static Sprite getSprite(String name){ return sprites.get(name); }
    
    public char getChar(){ return mapChar; }
    
    public String getType(){ return spriteType.name(); }
}