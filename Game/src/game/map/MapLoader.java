package game.map;

import io.InOut;
import java.io.IOException;
import java.io.InputStream;

public abstract class MapLoader{
    private static InOut term;
    
    public static void setIO(InOut term){ MapLoader.term = term; }
    
    public static char[][] load(String path){
        char[][] output = null;
        
        InputStream is = null;

        try{ is = MapLoader.class.getResourceAsStream(path); }
        catch(NullPointerException e){ if(term!=null)term.err("Failed to open map file '" + path + "'"); }
        
        if(is==null)if(term!=null)term.err("Failed to open map file '" + path + "'");
        
        int offset = 0;
        int available = getAvailable(is, path);
        StringBuilder inputBuilder = new StringBuilder();
        
        try{
            while(available>0){
                byte[] buffer = new byte[available];
                offset += is.read(buffer, offset, available);
                inputBuilder.append(new String(buffer));
                available = getAvailable(is, path);
            }
        }catch(IOException e){ if(term!=null)term.err("Failed to load from file '" + path + "'"); }
        
        String[] lines = inputBuilder.toString().split(System.lineSeparator());
        if(lines.length<2){
            lines = inputBuilder.toString().split("\n");
            if(lines.length<2){
                if(term!=null)term.err("Failed to read lines from file '" + path + "'");
            }
        }
        output = new char[Integer.parseInt(lines[1])][Integer.parseInt(lines[0])];
        for(int i=2; i<lines.length; i++){
            String[] temp = lines[i].split(",");
            for(int z=0; z<temp.length; z++){
                output[i-2][z] = (char)Integer.parseInt(temp[z]);
            }
        }
        
        return output;
    }
    
    private static int getAvailable(InputStream is, String path){
        try{ return is.available(); }
        catch(IOException e){
            if(term!=null)term.err("Failed to find available readable bytes for '" + path + "'");
            return 0;
        }
    }
}