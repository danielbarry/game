package game.map;

import game.Game;
import io.InOut;
import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONObject;
import sprites.Sprite;
import sprites.SpriteType;
import tiles.Tile;
import utils.loader.Loader;
import utils.math.Position;

public class Map{
    private InOut term;
    private char[][] map;
    private ArrayList<Tile> tiles = new ArrayList<>();
    
    private final HashMap<Character, Sprite> mapLoading = new HashMap(){{
        JSONArray array = Game.jHnd.getJSONObject("tiles").getJSONArray("sprites");
        for(int i=0; i<array.length(); i++){
            SpriteType st = new SpriteType((JSONObject)array.get(i));
            put(st.getChar(), st.getSprite());
        }
    }};

    /**
     * Simplifies getting the setting from the properties file as a character
     * <p>
     * @param str the setting to return
     * <p>
     * @return the setting as a character
     */
    private Character getSetting(String str){
        String setting = (String)Loader.getSetting(str);
        return (char)(Integer.parseInt(setting));
    }


    public Map(String path, InOut term){
        this.term = term;
        MapLoader.setIO(term);
        map = MapLoader.load(path);
        loadTiles();
        //Checking the map is loaded in correctly
//        tiles.forEach((tile) -> {
//            System.out.print(tile.getPosition() + " -- ");
//            int x = tile.getPosition().getX();
//            int y = tile.getPosition().getY();
//            System.out.println(map[y][x]);
//        });
    }


    /**
     * Loads the tiles to the <code>tiles</code> array
     */
    private void loadTiles(){
        for(int y = 0; y < getHeight(); y++){
            for(int x = 0; x < getWidth(); x++){
                tiles.add(getTile(x, y));
            }
        }
    }


    /**
     * Takes an x and y position and creates a tile with the correct sprite
     * and position, and returns it
     * <p>
     * @param x the x position of the tile
     * @param y the y position of the tile
     * <p>
     * @return the tile
     */
    private Tile getTile(int x, int y){
        Position p = new Position(x, y, 0);
        Sprite sprite = mapLoading.get(map[y][x]);
        Tile temp = new Tile(p, sprite);
        //@TODO: add the solid state to the tile
        temp.setPosition(p);
        return temp;
    }


    /**
     * Returns the map as a <code>char</code> array
     * <p>
     * @return the map as a <code>char</code> array
     */
    public char[][] getMapAsChar(){
        return map;
    }


    /**
     * Returns the map as an <code>ArrayList</code> of tiles
     * <p>
     * @return the map as an <code>ArrayList</code>F
     */
    public ArrayList<Tile> getMap(){
        return tiles;
    }


    /**
     * Gets the width of the map
     * <p>
     * @return the width of the map
     */
    public int getWidth(){
        return map[0].length;
    }


    /**
     * Gets the height of the map
     * <p>
     * @return the height of the map
     */
    public int getHeight(){
        return map.length;
    }
}
