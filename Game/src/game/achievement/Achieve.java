package game.achievement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Oliver
 */
public class Achieve{

    private final HashMap<String, Property> properties;
    private final HashMap<String, Achievement> achievements;


    private Achieve(){
        properties = new HashMap<>();
        achievements = new HashMap<>();
    }


    /**
     * Create and add a new property
     * <p>
     * @param name            the name of the property
     * @param initialValue    the initial value of the property
     * @param activationRule  the activation rule of the property
     * @param activationValue the activation value of the property
     * @param tags            the tags of the property, these are only relevant for resetting
     *                        multiple properties
     */
    public void defineProperty(String name, int initialValue, AchievementRule activationRule, int activationValue, String... tags){
        Property p = new Property(name, initialValue, activationRule, activationValue, tags);
        properties.put(name, p);
    }


    /**
     * Create and add a new achievement
     * <p>
     * @param name  the name of the achievement
     * @param props one or more properties which
     *              need to be met to unlock the
     *              achievementF
     */
    public void defineAchievement(String name, Property... props){
        Achievement a = new Achievement(name, props);
        achievements.put(name, a);
    }


    /**
     * Gets the current value of the property requested
     * <p>
     * @pre <code>propertyExists(name)</code>
     * <p>
     * @param name the name of the property to get the value of
     * <p>
     * @return the value of that property
     */
    public int getValue(String name){
        return properties.get(name).getValue();
    }


    /**
     * Returns <code>true</code> if all the properties are stored in the achieve class
     * <p>
     * @param names the names of the properties to check
     * <p>
     * @return <code>true</code> if all the properties exists
     */
    public boolean propertiesExist(String... names){
        boolean exists = true;
        for(String name : names){
            exists &= properties.containsKey(name);
        }
        return exists;
    }


    /**
     * Returns <code>true</code> if all the achievements are stored in the achieve class
     * <p>
     * @param names the names of the achievements
     * <p>
     * @return <code>true</code> if all the achievements exists
     */
    public boolean achievementExists(String... names){
        boolean exists = true;
        for(String name : names){
            exists &= achievements.containsKey(name);
        }
        return exists;
    }


    /**
     * Sets the value of the property with the name <code>name</code>
     * only sets the value of the property if the value meets these conditions:
     * <p>
     * <code>
     *  if the property rule is "Greater than" then:</code><br><code>
     *      the value must be greater than the current value of the property
     * </code><p>
     * <code>
     *  if the property rule is "Less than" then:</code><br><code>
     *      the value must be less than the current value of the property
     * </code><p>
     * if both of these are met, or the rule is <code>"Equal to"</code>
     * then the property value is updated to the new value
     * <p>
     * this should stop errors like if the player kills 10 people with a
     * bomb and unlock an achievement, then later they might kill another
     * 3 people with a bomb, and that could lock the achievement again
     * <p>
     * @pre <code>propertyExists(name)</code>
     * <p>
     * @param value the value to update the value to
     * @param name  the name of the property to update
     */
    public void setValue(int value, String name){
        Property p = properties.get(name);
        switch(p.getRule()){
            case GREATER_THAN:
                value = value > p.getValue() ? value : p.getValue();
                break;
            case LESS_THAN:
                value = value < p.getValue() ? value : p.getValue();
                break;
        }
        p.setValue(value);
    }


    /**
     * Returns the property with the given name
     * <p>
     * @pre <code>propertiesExist(name)</code>
     * <p>
     * @param name the name of the property to return
     * <p>
     * @return the property
     */
    public Property getProperty(String name){
        return properties.get(name);
    }

    /**
     * Returns the achievement with the given name
     * <p>
     * @pre <code>achievementsExist(name)</code>
     * <p>
     * @param name the name of the achievement to return
     * <p>
     * @return the Achievement
     */
    public Achievement getAchievement(String name){
        return achievements.get(name);
    }
    

    /**
     * Adds a value to one or more properties
     * <p>
     * @pre <code>propertiesExist(names)</code>
     * <p>
     * @param value the value to add to the properties
     * @param names the names of the properties
     */
    public void addValue(int value, String... names){
        for(String str : names){
            setValue(getValue(str) + value, str);
        }
    }


    /**
     * Checks all the achievements to see if all of the properties are active, if they are,
     * the achievement is unlocked
     * <p>
     * @return an array of the just unlocked achievements
     */
    public Achievement[] checkAchievements(){
        ArrayList<Achievement> unlocked = new ArrayList<>();
        achievements.values().parallelStream().filter((ach) -> !ach.isUnlocked()).forEach((ach) -> {
            int activeProperties = 0;
            for(Property prop : ach.getProperties()){
                if(prop.isActive()){
                    activeProperties++;
                }
            }
            if(activeProperties == ach.getProperties().length){
                ach.setUnlocked(true);
                unlocked.add(ach);
            }
        });
        //For some reason simply casting an Object[] to an Achievement[] causes an error, so this fices it
        return Arrays.copyOf(unlocked.toArray(), unlocked.size(), Achievement[].class);
    }


    /**
     * Resets a property to its original value
     * <p>
     * @pre <code>propertiesExist(name)</code>
     * <p>
     * @param names the names of the properties to reset
     */
    public void resetProperties(String... names){
        for(String name : names){
            Property p = getProperty(name);
            if(p != null){
                p.setValue(p.INITIAL_VALUE);
            }
        }
    }


    /**
     * Resets all properties with one or more of the passed tags to their original value
     * <p>
     * @param tags the tags of all the properties to reset
     */
    public void resetTaggedProperties(String... tags){
        if(tags.length == 0){ //don't want to accidentally reset all properties!
            return;
        }
        properties.values().parallelStream().filter((p) -> p.containsTags(tags)).forEach((p) -> p.setValue(p.INITIAL_VALUE));
    }


    /**
     * Returns <code>true</code> if the achievement is unlocked
     * <p>
     * @param name the name of the achievement to check
     * <p>
     * @return <code>true</code> if the achievement is unlocked
     */
    public boolean isAchievementUnlocked(String name){
        return achievements.get(name).isUnlocked();
    }


    /**
     * Returns <code>true</code> if the property is active
     * <p>
     * @param name the name of the property to check
     * <p>
     * @return <code>true</code> if the property is active
     */
    public boolean isPropertyActive(String name){
        return properties.get(name).isActive();
    }


    /**
     * Returns an <code>Array</code> containing all the unlocked achievements
     * <p>
     * @return an <code>Array</code> containing all the unlocked achievements
     */
    public Achievement[] getUnlockedAchievements(){
        Object[] temp = achievements.values().parallelStream().filter(a -> a.isUnlocked()).toArray();
        return Arrays.copyOf(temp, temp.length, Achievement[].class);
    }


    /**
     * Returns an <code>Array</code> containing all the locked achievements
     * <p>
     * @return an <code>Array</code> containing all the locked achievements
     */
    public Achievement[] getLockedAchievements(){
        Object[] temp = achievements.values().parallelStream().filter(a -> !a.isUnlocked()).toArray();
        return Arrays.copyOf(temp, temp.length, Achievement[].class);
    }
    
    public Achievement[] getAllAchievements(){
        Object[] temp = achievements.values().toArray();
        return Arrays.copyOf(temp, temp.length, Achievement[].class);
    }


    /**
     * Returns an <code>Array</code> containing all the inactive properties
     * <p>
     * @return an <code>Array</code> containing all the inactive properties
     */
    public Property[] getInactiveProperties(){
        Object[] temp = properties.values().parallelStream().filter(p -> !p.isActive()).toArray();
        return Arrays.copyOf(temp, temp.length, Property[].class);
    }


    /**
     * Returns an <code>Array</code> containing all the active properties
     * <p>
     * @return an <code>Array</code> containing all the active properties
     */
    public Property[] getActiveProperties(){
        Object[] temp = properties.values().parallelStream().filter(p -> p.isActive()).toArray();
        return Arrays.copyOf(temp, temp.length, Property[].class);
    }
    
    public Property[] getAllProperties(){
        Object[] temp = properties.values().toArray();
        return Arrays.copyOf(temp, temp.length, Property[].class);
    }

    public enum AchievementRule{

        GREATER_THAN("Greater than"), LESS_THAN("Less than"), EQUAL_TO("Equal to");

        private final String val;


        private AchievementRule(String val){
            this.val = val;
        }


        @Override
        public String toString(){
            return val;
        }
    }

    private static Achieve instance;


    public static Achieve getInstance(){
        if(instance == null){
            instance = new Achieve();
        }
        return instance;
    }
}
