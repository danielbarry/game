package game.achievement;

/**
 *
 * @author Oliver
 */
public class Achievement{

    private final String NAME;
    private final Property[] PROPERTIES;
    private boolean unlocked;


    /**
     * Creates a new achievement, only accessible by Achieve
     * <p>
     * @param name       the name of the achievement
     * @param properties the array of properties which
     *                   all need to be met to unlock
     *                   the achievement
     */
    protected Achievement(String name, Property... properties){
        this.NAME = name;
        this.PROPERTIES = properties;
        //@TODO: save achievment states to saved game, and read in when started
        this.unlocked = false;
    }


    /**
     * Returns <code>true</code> if the achievement is unlocked
     * <p>
     * @return <code>true</code> if the achievement is unlocked
     */
    public boolean isUnlocked(){
        return unlocked;
    }


    public void setUnlocked(boolean unlocked){
        this.unlocked = unlocked;
    }


    public String getName(){
        return NAME;
    }


    public Property[] getProperties(){
        return PROPERTIES;
    }


    @Override
    public String toString(){
        return NAME + " is " + (isUnlocked() ? "" : " not ") + "unlocked.";
    }
}
