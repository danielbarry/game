package game.achievement;

import game.achievement.Achieve.AchievementRule;
import static game.achievement.Achieve.AchievementRule.GREATER_THAN;
import static game.achievement.Achieve.AchievementRule.LESS_THAN;

/**
 *
 * @author Oliver
 */
public class Property{

    private final String NAME;
    private int value;
    private final AchievementRule ACTIVATION_RULE;
    private final int ACTIVATION_VALUE;
    public final int INITIAL_VALUE;
    private final String[] tags;


    /**
     * Creates a new property with the given values, only accessible by Achieve
     * <p>
     * @param name            the name of the property
     * @param initialValue    the initial value of the property
     * @param activationRule  the activation rule, can be something like <code>active if greater than</code>
     * @param activationValue the value the rule needs to reach to activate the property
     * @param tags            some optional tags used to make it easier to reset multiple properties at once
     *                        these could be used if some properties are meant to have a time constraint, or
     *                        they can only be unlocked in a certain level and if you reach the end of the level,
     *                        they get reset
     */
    public Property(String name, int initialValue, AchievementRule activationRule, int activationValue, String... tags){
        this.NAME = name;
        this.value = initialValue;
        this.INITIAL_VALUE = initialValue;
        this.ACTIVATION_RULE = activationRule;
        this.ACTIVATION_VALUE = activationValue;
        this.tags = tags;
    }


    /**
     * Returns the current value of the property
     * <p>
     * @return the current value of the property
     */
    public int getValue(){
        return value;
    }


    public String getName(){
        return NAME;
    }


    public AchievementRule getRule(){
        return ACTIVATION_RULE;
    }


    /**
     * Sets the value of the property to the given value
     * <p>
     * @param value the new value of the property
     */
    public void setValue(int value){
        this.value = value;
    }


    public boolean isActive(){
        switch(ACTIVATION_RULE){
            case EQUAL_TO:
                return value == ACTIVATION_VALUE;
            case GREATER_THAN:
                return value > ACTIVATION_VALUE;
            case LESS_THAN:
                return value < ACTIVATION_VALUE;
        }
        return false;
    }


    public String[] getTags(){
        return tags;
    }


    public boolean containsTags(String... tags){
        if(tags.length == 0){ //if there aren't any tags to check, then it matches
            return true;
        }
        if(this.tags.length == 0){ //if there arent any tags in the property, and there are some to check, then we don't want to update it
            return false;
        }
        for(String tag1 : this.tags){
            for(String tag2 : tags){
                return tag1.equals(tag2);
            }
        }
        return false;
    }


    @Override
    public String toString(){
        return "\"" + NAME + "\" -Value=\"" + value + "\" -Rule=\"" + ACTIVATION_RULE
                + "\" -Activation=\"" + ACTIVATION_VALUE + "\" is" + (isActive() ? " " : " not ") + "active";
    }
}
