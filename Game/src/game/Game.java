package game;

import entities.Entity;
import entities.mob.Player;
import game.map.Map;
import io.Gui;
import io.InOut;
import io.Terminal;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.json.JSONArray;
import sprites.Sprite;
import utils.loader.Loader;
import utils.math.Position;
import utils.settings.JsonHandler;
import utils.updater.Updater;

/**
 * 
 * @author dan
 * @version 1.2
 */
public class Game{
    public static JsonHandler jHnd = new JsonHandler("settings.json");
    
    //Variables
    private InOut io;
    private InOut term = null;
    private int currentMap = 0;
    private Map[] maps;
    private Entity[] entities = new Entity[0];
    Player player = new Player("Player One", new Position(1, 1, 0d), new Sprite(16, 0xFF00FF));
    private boolean running = true;
    private Boolean drawReady = false;
    private long sleepTime = 1000 / jHnd.getLong("physics.ups");
    
    public static void main(String[] args){ new Game(); }
    
    public Game(){
        //Setup Terminal if console exists,
        //only returns an instance if the System.console() exists
        term = Terminal.getInstance();
        
        //Diplay Version and Update
        if(term!=null)term.debug(Updater.getVersion());
        new Updater();
        
        //Setup settings
        Loader.setInOut(term);
        
        //Setup maps
        initMaps();
        
        if(term!=null)term.debug(("'map1' loaded with width:" + maps[0].getWidth() + " height:" + maps[0].getHeight()));
        
        //TODO: Remove this test
        char[][] m = maps[currentMap].getMapAsChar();
        if(term!=null)for(char[] ca : m)term.debug(new String(ca));
        
        //Create GUI
        io = new Gui(maps,entities,player);
        try {
            if(term!=null)term.debug("GUI loading...");
            SwingUtilities.invokeAndWait((Runnable) io);
            if(term!=null)term.debug("GUI loaded!");
        } catch (InterruptedException ex) {
            if(term!=null)term.debug("InterruptedException from invoke and wait on GUI start");
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvocationTargetException ex) {
            if(term!=null)term.debug("InvocationTargetException from invoke and wait on GUI start");
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //Run physics loop
        doPhysics();
    }
    
    private void initMaps(){
        String location = jHnd.getString("maps.location");
        JSONArray mapArray = jHnd.getJSONArray("maps.files");
        maps = new Map[mapArray.length()];
        for(int i=0; i<mapArray.length(); i++)maps[i] = new Map(location + (String)mapArray.get(i), term);
    }
    
    private void doPhysics(){
        if(term!=null)term.debug("Physics loop reached!");
        while(running){
            long sleepStart = System.currentTimeMillis();
            //if(term!=null)term.debug("update physics"); //NOTE:Slows operation so commented out
            
            //Physics Update
            for(Entity e : entities)e.updatePhysics();
            
            //Instant View Update
            //TODO:Update game as soon as it is available
            synchronized(drawReady){
                drawReady = false;
                drawReady = true;
            }
            
            //TODO: Carry over error of time lost so that next frame is
            //      responsible for catching up. This reduces the effects of lag
            //      as shown in the "Rival Game" repository.
            //Sleep Section
            //NOTE: Sleep for correct amount of time
            while(System.currentTimeMillis() - sleepStart < sleepTime){
                long time = sleepTime - System.currentTimeMillis() - sleepStart;
                if(time > 0){
                    try{ Thread.sleep(time); }
                    catch(InterruptedException e){}
                }
            }
        }
    }
    
    public void end(){ running = false; }
    
    //TODO: Replace with an array of tiles and depreciate this method
    public char[][] getView(){
        char[][] view;
        synchronized(drawReady){
            drawReady = false;
            //TODO: Restrict the view
            view = maps[currentMap].getMapAsChar();
            drawReady = true;
        }
        return view;
    }
    
    /**
     * Return the full map as a one dimension char array.
     * @return a char array containing the whole map.
     * 
     * @deprecated This method doesn't give x or y positions and was implemented
     * as a test. It is highly not recommended to use this method and it will be
     * removed in later versions. It remains for testing purposes.
     */
    public char[] getViewLinear(){
        Map m = maps[currentMap];
        char[] bMap;
        synchronized(drawReady){
            drawReady = false;
            char[][] map = m.getMapAsChar();
            bMap = new char[m.getWidth() * m.getHeight()];
            for(int i=0; i<map.length; i++)for(int z=0; z<map[i].length; z++)bMap[(i*map[i].length)+z] = map[i][z];
            drawReady = true;
        }
        return bMap;
    }
}