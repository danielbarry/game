package entities.mob;

import entities.Entity;
import entities.weapons.Weapon;
import sprites.Sprite;
import utils.math.Position;

/**
 * A mob is any mobile character, either player controlled or npc,
 * all mobs have a name, an active weapon and some health
 * <p>
 * @author Oliver
 */
public class Mob extends Entity{

    protected Weapon activeWeapon;
    private int fistDamage = 1;
    private String name;


    /**
     * Creates a new mob with the given amount of
     * health and the given name, at the given x and
     * y coordinates on the game grid
     * <p>
     * @param name   the name of the entity
     * @param health the health the entity has
     * @param v      the position and rotation
     * @param sprite the sprite of the mob
     */
    public Mob(String name, int health, Position v, Sprite sprite){
        super(health, true, v, sprite);
        this.name = name;
    }


    /**
     * Returns the weapon currently active
     * <p>
     * @return the currently active weapon
     */
    public Weapon getActiveWeapon(){
        return activeWeapon;
    }


    /**
     * Returns the damage unarmed fists can do
     * <p>
     * @return the damage the fists will do
     */
    public int getFistDamage(){
        return fistDamage;
    }


    /**
     * Returns the damage of the active activeWeapon,
     * if no activeWeapon is active, then the damage
     * of the fists is used
     * <p>
     * @return the damage of the activeWeapon or the fists
     */
    public int getWeaponDamage(){
        if(activeWeapon != null){
            return activeWeapon.getDamage();
        }
        return fistDamage;
    }


    /**
     * Removes the active activeWeapon from the mob
     */
    public void removeActiveWeapon(){
        activeWeapon = null;
    }


    /**
     * Sets the damage of an unarmed mob
     * <p>
     * @param damage the damage to set
     */
    public void setFistDamage(int damage){
        this.fistDamage = damage;
    }


    /**
     * Sets the active weapon to the given weapon
     * <p>
     * @param weapon the weapon to be used
     */
    public void setWeapon(Weapon weapon){
        this.activeWeapon = weapon;
    }


    @Override
    public String toString(){
        return "\nName:                  " + name
                + "\nActive weapon:         " + (activeWeapon == null ? "Fists" : activeWeapon.getName())
                + "\n" + super.toString();
    }

}
