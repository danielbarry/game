package entities.mob;

import entities.weapons.Weapon;
import java.util.ArrayList;
import sprites.Sprite;
import utils.math.Position;

/**
 * A player is a Mob that a human controls, all players inherit all the attributes
 * from Mob, and player can also carry more than 1 weapon, and players can also have a score
 * @author Oliver
 */
public class Player extends Mob{

    private static final int START_HEALTH = 20;
    private int arsenalSize = 10;
    private ArrayList<Weapon> carriedWeapons = new ArrayList<>();
    private int fistDamage = 1;
    private int score;


    /**
     * Creates a new player, by default
     * they start with 20 health, but this
     * can be upgraded
     * <p>
     * @param name   the name of the player
     * @param v      the position and rotation
     * @param sprite the sprite of the player
     */
    public Player(String name, Position v, Sprite sprite){
        super(name, START_HEALTH, v, sprite);
    }


    /**
     * Drops the weapon if it exists in the arsenal
     * <p>
     * @param weapon the weapon to drop
     */
    public void dropWeapon(Weapon weapon){
        if(activeWeapon == weapon){
            removeActiveWeapon();
        }
        carriedWeapons.remove(weapon);
    }


    /**
     * Returns an <code>ArrayList</code> of all of the weapons the player is holding
     * <p>
     * @return an <code>ArrayList</code> of all of the weapons the player is holding
     */
    public ArrayList<Weapon> getAresenal(){
        return carriedWeapons;
    }


    /**
     * Returns the current score of the player
     * <p>
     * @return the current score of the player
     */
    public int getScore(){
        return score;
    }


    /**
     * Returns the number of weapons being carried by the mob
     * <p>
     * @return the number of weapons carried
     */
    public int numberOfWeapons(){
        return carriedWeapons.size();
    }


    /**
     * Picks up the given weapon and adds it to the
     * mobs arsenal, can only pick up the weapon if
     * the mobs arsenal is not full
     * <p>
     * @param weapon the weapon to be picked up
     */
    public void pickupWeapon(Weapon weapon){
        if(carriedWeapons.size() < arsenalSize){
            carriedWeapons.add(weapon);
        }
    }


    /**
     * Sets the active weapon to the given weapon,
     * only weapons carried by the mob can be set
     * <p>
     * @param weapon the weapon the mob has active
     */
    public void setActiveWeapon(Weapon weapon){
        if(carriedWeapons.contains(weapon)){
            setWeapon(weapon);
        }
    }


    /**
     * Sets the size of the arsenal the mob can hold, if this
     * is set lower than the current arsenal size, the most
     * recent weapons will be dropped until there is enough room
     * <p>
     * @param size the new size of the arsenal
     */
    public void setArsenalSize(int size){
        arsenalSize = size;
        if(carriedWeapons.size() > arsenalSize){
            for(int i = arsenalSize; i <= carriedWeapons.size(); i++){
                carriedWeapons.remove(i);
            }
        }
    }


    /**
     * Sets the score the given value
     * <p>
     * @param score the score to set
     */
    public void setScore(int score){
        this.score = score;
    }


    @Override
    public String toString(){
        return super.toString()
                + "\nAresenal size:         " + arsenalSize
                + "\nCurrent aresenal size: " + numberOfWeapons()
                + "\nScore:                 " + score;
    }
}
