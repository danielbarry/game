package entities;

import sprites.Sprite;
import utils.math.Position;

/**
 *
 * @author Oliver
 */
public abstract class Entity{

    private int currentHealth;
    private final int maxHealth;
    private Position pos;
    private boolean solid;
    private final Sprite SPRITE;


    /**
     * Creates a new entity with the given parameters
     * the current health is set the max health as it
     * is created
     * <p>
     * @param health  the maximum health of the entity
     * @param isSolid if the entity is solid or not
     * @param v       the position and rotation
     * @param sprite  the sprite of the entity
     */
    public Entity(int health, boolean isSolid, Position v, Sprite sprite){
        maxHealth = health;
        currentHealth = health;
        solid = isSolid;
        this.pos = v;
        this.SPRITE = sprite;
    }


    /**
     * Adds the <code>amount</code> of health to the entity
     * <p>
     * @param amount the amount of health to be added
     */
    public void addHealth(int amount){
        currentHealth += amount;
    }


    /**
     * Returns the health remaining of the entity
     * <p>
     * @return the health of the entity
     */
    public int getHealth(){
        return currentHealth;
    }


    /**
     * Returns the max default health of the entity
     * <p>
     * @return the max health of the entity without any powerups
     */
    public int getMaxHealth(){
        return maxHealth;
    }


    /**
     * Returns the position of the entity in a Position
     * <p>
     * @return the position and rotation of the entity
     */
    public Position getPosition(){
        return pos;
    }


    /**
     * Gets the sprite to render to the screen
     * <p>
     * @return the sprite to render
     */
    public Sprite getSprite(){
        return SPRITE;
    }


    /**
     * Returns if the entity is dead or not
     * <p>
     * @return <code>true</code> if the current health is less than or equal to
     *         0
     */
    public boolean isDead(){
        return currentHealth <= 0;
    }


    /**
     * Returns <code>true</code> if the entity is solid
     * <p>
     * @return <code>true</code> if the entity is solid
     */
    public boolean isSolid(){
        return solid;
    }


    /**
     * Removes the <code>amount</code> of health from the entity
     * <p>
     * @param amount the amount of health to remove
     */
    public void removeHealth(int amount){
        currentHealth -= amount;
    }


    /**
     * Sets the position of the entity
     * <p>
     * @param pos the new position
     */
    public void setPosition(Position pos){
        this.pos = pos;
    }


    @Override
    public String toString(){
        return "Health:                " + currentHealth
                + "\nMax health:            " + maxHealth
                + "\nSolid?                 " + isSolid()
                + "\nDead?                  " + isDead();
    }


    /**
     * Is called per game physics update, where each entity is
     * responsible for updating it's own physics relative to the
     * other entities.
     */
    public void updatePhysics(){
    }
}
