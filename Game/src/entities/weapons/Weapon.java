package entities.weapons;

import entities.Entity;
import sprites.Sprite;
import utils.math.Position;

/**
 *
 * @author Oliver
 */
public class Weapon extends Entity{

    private String name;
    private int damage;
    private final int coolDown;


    /**
     * Creates a new weapon with the given name
     * weapons are given the maximum possible health
     * <p>
     * @param name     the name of the weapon
     * @param damage   the damage the weapon does
     * @param v        the position and rotation
     * @param sprite   the sprite the weapon will have
     * @param coolDown the cooldown period of the weapon
     */
    public Weapon(String name, int damage, Position v, Sprite sprite, int coolDown){
        super(Integer.MAX_VALUE, false, v, sprite);
        this.name = name;
        this.damage = damage;
        this.coolDown = coolDown;
    }


    /**
     * The ticks needed to wait between attacks
     * <p>
     * @return the cool down time of the weapon
     */
    public int getCoolDown(){
        return coolDown;
    }


    /**
     * Returns the damage value of the weapon
     * <p>
     * @return the damage the weapon causes
     */
    public int getDamage(){
        return damage;
    }


    /**
     * Returns the name of the weapon
     * <p>
     * @return the name of the weapon
     */
    public String getName(){
        return name;
    }


    /**
     * Sets the damage of the weapon, useful for upgrading
     * <p>
     * @param damage the damage to set the weapon to
     */
    public void setDamage(int damage){
        this.damage = damage;
    }


    @Override
    public String toString(){
        return "Name:                  " + name
                + "\nDamage:                " + damage
                + "\nCoolDown:              " + coolDown;
    }
}
